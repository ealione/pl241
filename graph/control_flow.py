# -*- encoding: utf-8 -*-
from __future__ import annotations

from graphviz import Digraph

from compiler import Bytecode
from objects import CompiledFunctionObject, CompiledProcedureObject


def control_flow_graph(block_code: Bytecode):
    g = Digraph('G', filename='control_flow.gv')
    g.attr(fontname='consolas')

    for i, block in enumerate(block_code.blocks):
        label = str(block_code.instructions_in_block(i)).replace('\n', '\l')
        g.node(f"{i}", label=label + '\l', shape='box', fontname='helvetica')
        for children in block.children:
            g.edge(f"{i}", f"{children}")

    for c in block_code.constants:
        if isinstance(c, CompiledFunctionObject) or isinstance(c, CompiledProcedureObject):
            with g.subgraph(name=c.symbol.name) as s:
                s.attr(style='filled', color='lightgrey')

                for i, block in enumerate(c.bytecode.blocks):
                    label = str(c.bytecode.instructions_in_block(i)).replace('\n', '\l')
                    s.node(f"{c.symbol.name}{i}", label=label + '\l', shape='box', fontname='helvetica')
                    for children in block.children:
                        s.edge(f"{c.symbol.name}{i}", f"{c.symbol.name}{children}")

                s.attr(label=c.symbol.name)

    g.view()
