# -*- encoding: utf-8 -*-
import glob
import os.path as path

from tokens import Position


# TODO: maybe use the rich module
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def read_whole_file(f: str) -> str:
    with open(f, 'r') as f:
        return f.read()


def readfile(f: str) -> str:
    with open(f) as infile:
        for line in infile:
            yield line


def get_filelist(datadirectory: str, file_type: str = 'txt') -> glob:
    return glob.iglob(path.join(datadirectory, "*.{}".format(file_type)), recursive=False)


def highlight(source):
    # TODO: This is temporary
    source = source.replace("main", f"{bcolors.OKGREEN}main{bcolors.ENDC}")
    source = source.replace("var", f"{bcolors.HEADER}var{bcolors.ENDC}")
    source = source.replace("array", f"{bcolors.HEADER}array{bcolors.ENDC}")
    source = source.replace("call", f"{bcolors.OKBLUE}call{bcolors.ENDC}")
    source = source.replace("return", f"{bcolors.HEADER}return{bcolors.ENDC}")
    source = source.replace("while", f"{bcolors.HEADER}while{bcolors.ENDC}")
    source = source.replace("if", f"{bcolors.HEADER}if{bcolors.ENDC}")
    source = source.replace("else", f"{bcolors.HEADER}else{bcolors.ENDC}")
    source = source.replace("let", f"{bcolors.HEADER}let{bcolors.ENDC}")
    source = source.replace("=", f"{bcolors.FAIL}={bcolors.ENDC}")
    source = source.replace("+", f"{bcolors.FAIL}+{bcolors.ENDC}")
    source = source.replace("-", f"{bcolors.FAIL}-{bcolors.ENDC}")
    source = source.replace("/", f"{bcolors.FAIL}/{bcolors.ENDC}")
    source = source.replace("<-", f"{bcolors.FAIL}<-{bcolors.ENDC}")
    source = source.replace(">=", f"{bcolors.FAIL}>={bcolors.ENDC}")
    source = source.replace("<=", f"{bcolors.FAIL}<={bcolors.ENDC}")
    return source


def mark_token(text: str, pos_start: Position, pos_end: Position) -> str:
    result = ''

    idx_start = max(text.rfind('\n', 0, pos_start.current), 0)
    idx_end = text.find('\n', idx_start + 1)
    if idx_end < 0:
        idx_end = len(text)

    line_count = pos_end.line - pos_start.line + 1
    for i in range(line_count):
        line = text[idx_start:idx_end]
        col_start = pos_start.column - 1 if i == 0 else 0
        col_end = pos_end.column - 1 if i == line_count - 1 else len(line) - 1

        result += line + '\n'
        result += ' ' * col_start + bcolors.WARNING + '^' * (col_end - col_start) + bcolors.ENDC

        idx_start = idx_end
        idx_end = text.find('\n', idx_start + 1)
        if idx_end < 0:
            idx_end = len(text)

    return result.replace('\t', '')


def progress(iteration, total, prefix='', suffix='', decimals=1, length=100, fill='█', end="\r"):
    """
    Call in a loop to create terminal progress bar
    @params:
        iteration   - Required  : current iteration (Int)
        total       - Required  : total iterations (Int)
        prefix      - Optional  : prefix string (Str)
        suffix      - Optional  : suffix string (Str)
        decimals    - Optional  : positive number of decimals in percent complete (Int)
        length      - Optional  : character length of bar (Int)
        fill        - Optional  : bar fill character (Str)
        end         - Optional  : end character (e.g. "\r", "\r\n") (Str)

    @usage:
    >>> import time
    >>> items = list(range(0, 57))
    >>> l = len(items)
    >>> progress(0, l, prefix='Progress:', suffix='Complete', length=50)
    >>> for i, item in enumerate(items):
    >>>     # Do stuff...
    >>>     time.sleep(0.1)
    >>>     # Update Progress Bar
    >>>     progress(i + 1, l, prefix='Progress:', suffix='Complete', length=50)
    """
    percent = ("{0:." + str(decimals) + "f}").format(100 * (iteration / float(total)))
    filledLength = int(length * iteration // total)
    bar = fill * filledLength + '-' * (length - filledLength)
    print('\r%s |%s| %s%% %s' % (prefix, bar, percent, suffix), end=end)
    if iteration == total:
        print()
