# -*- encoding: utf-8 -*-
from copy import copy

from ast import Statement
from tokens import Position
from utils import mark_token, bcolors


class Error:
    def __init__(self, start: Position, end: Position, name: str, details: str, source: str, filename: str):
        self.start = copy(start)
        self.end = copy(end)
        self.name = name
        self.details = details
        self.source = source
        self.filename = filename

    def __str__(self) -> str:
        result = f'{bcolors.BOLD}FILE:{bcolors.ENDC} {bcolors.UNDERLINE}{bcolors.HEADER}{self.filename}{bcolors.ENDC}{bcolors.ENDC}, {bcolors.BOLD}LINE:{bcolors.ENDC} [{self.start.line + 1}]\n'
        result += f'{bcolors.FAIL}{self.name}{bcolors.ENDC}: {self.details}\n'
        result += f'{mark_token(self.source, self.start, self.end)}\n'
        return result


class IllegalCharError(Error):
    def __init__(self, start: Position, end: Position, details: str, source: str, filename: str = 'UNKNOWN'):
        super().__init__(start, end, 'Illegal Character', details, source, filename)


class ExpectedCharError(Error):
    def __init__(self, start: Position, end: Position, details: str, source: str, filename: str = 'UNKNOWN'):
        super().__init__(start, end, 'Expected Character', details, source, filename)


class InvalidSyntaxError(Error):
    def __init__(self, start: Position, end: Position, details: str, source: str, filename: str = 'UNKNOWN'):
        super().__init__(start, end, 'Invalid Syntax', details, source, filename)


class CompilationError:
    def __init__(self, statement: Statement, details: str):
        self.statement = statement
        self.details = details

    def __str__(self) -> str:
        return f"Compilation Error: {str(self.statement)} {self.details}"
