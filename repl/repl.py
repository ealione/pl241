# -*- encoding: utf-8 -*-
from __future__ import absolute_import

import os.path as path
from typing import Tuple, List, Optional

from _exceptions import InvalidSyntaxError, CompilationError
from ast import Program
from compiler import Compiler, Bytecode
from lexer import Lexer
from objects import Object
from parser import Parser
from utils import read_whole_file


class Repl:
    @staticmethod
    def run_file(f: str) -> Optional[
        Tuple[Program, List[InvalidSyntaxError], Bytecode, List[Object], CompilationError]]:
        if not path.isfile(f):
            raise ValueError(f"Given file [{f}] not found!")
        source = read_whole_file(f)

        lex = Lexer(source, f)
        parser = Parser(lex)
        program = parser.parse_program()

        compiler = Compiler()
        compilation_errors = compiler.compile(program)
        compiled_code = compiler.byte_code
        constants = compiler.constants
        return program, parser.errors, compiled_code, constants, compilation_errors
