# -*- encoding: utf-8 -*-
from unittest import TestCase

from parse_lines import ReplParser

from ast import *


class TestParer(TestCase):

    def test_if_statements(self):
        lines = [
            "if x < y then",
            "   let x <- x + 5",
            "else",
            "   let x <- x - 5",
            "fi"
        ]

        p = ReplParser()

        for line in lines:
            p.parse(line)

        program = p.program
        self.assertEqual(len(program), 1)
        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, IfExpression))

        self.infix_expression_test_helper(exp.condition, "x", "<", "y")

        self.assertEqual(len(exp.consequence.statements), 1)

        consequence = exp.consequence.statements[0]
        self.assertTrue(isinstance(consequence, LetStatement))

        self.let_statement_test_helper(consequence, "x")

        self.assertTrue(isinstance(exp.alternative, BlockStatement))

    def test_while_statements(self):
        lines = [
            "while x < y do",
            "   let x <- x + 5",
            "   let y <- x - 5",
            "od;"
        ]

        p = ReplParser()

        for line in lines:
            p.parse(line)

        program = p.program
        self.assertEqual(len(program), 1)
        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, WhileExpression))

        self.infix_expression_test_helper(exp.condition, "x", "<", "y")

        self.assertTrue(isinstance(exp.body, BlockStatement))
        self.assertEqual(len(exp.body.statements), 2)

        statement_1 = exp.body.statements[0]
        statement_2 = exp.body.statements[1]
        self.assertTrue(isinstance(statement_1, LetStatement))
        self.assertTrue(isinstance(statement_2, LetStatement))

        self.let_statement_test_helper(statement_1, "x")
        self.let_statement_test_helper(statement_2, "y")

    def test_multiple_statements(self):
        lines = [
            "let x <- x + 5",
            "let y <- x - 5"
        ]

        expected = [
            "x",
            "y"
        ]

        p = ReplParser()

        for line in lines:
            p.parse(line)

        program = p.program
        self.assertEqual(len(program), 2)

        for i, stmt in enumerate(program):
            self.assertTrue(isinstance(stmt, LetStatement))
            self.let_statement_test_helper(stmt, expected[i])

    def test_function_statements(self):
        lines = [
            "function declarations(i, j)",
            "   var x, y; ",
            "   array[10] arr1, arr2;",
            "{",
            "   let g <- 5;",
            "};"
        ]

        test = [['i', 'j'], [['x', 'y'], ['arr1', 'arr2']], 1]

        p = ReplParser()

        for line in lines:
            p.parse(line)

        stmt = p.program[0]
        function = stmt.expression

        self.assertTrue(isinstance(function, FunctionLiteral))

        parameters = function.parameters
        self.assertEqual(len(test[0]), len(parameters))
        for i, ident in enumerate(test[0]):
            self.literal_expression_test_helper(parameters[i], ident)

        declarations = function.declarations
        self.assertEqual(len(test[1]), len(declarations))
        for i, decl in enumerate(test[1]):
            self.assertEqual(len(decl), len(declarations[i]))
            for j, ident in enumerate(decl):
                self.literal_expression_test_helper(declarations[i][j], ident)

        body = function.body
        self.assertEqual(test[2], len(body.statements))

    def test_procedure_statements(self):
        lines = [
            "procedure declarations;",
            "   var x; ",
            "   var i, j",
            "{",
            "   let g <- 5;",
            "};"
        ]

        test = [1, 2]

        p = ReplParser()

        for line in lines:
            p.parse(line)

        program = p.program

        stmt = program[0]
        function = stmt.expression

        self.assertEqual(len(test), len(function.declarations))

        for i, ident in enumerate(test):
            self.assertEqual(ident, len(function.declarations[i]))

    def let_statement_test_helper(self, s: LetStatement, name: str):
        self.assertEqual('let', s.token_literal())
        self.assertTrue(isinstance(s, LetStatement))
        self.assertEqual(name, s.name.token_literal())
        self.assertEqual(name, s.name.expression)

    def infix_expression_test_helper(self, exp: InfixExpression, left: Union[str, int], operator: str,
                                     right: Union[str, int]):
        self.assertTrue(isinstance(exp, InfixExpression))

        self.literal_expression_test_helper(exp.left, left)
        self.assertEqual(operator, exp.operator)
        self.literal_expression_test_helper(exp.right, right)

    def literal_expression_test_helper(self, exp: Union[IntegerLiteral, Identifier, Boolean],
                                       expected: Union[str, int, bool]):
        self.assertIn(type(expected), [str, int, bool])

        if isinstance(expected, str):
            self.identifier_test_helper(exp, expected)
        elif isinstance(expected, bool):
            self.boolean_literal_test_helper(exp, expected)
        elif isinstance(expected, int):
            self.integer_literal_test_helper(exp, expected)

    def integer_literal_test_helper(self, il: IntegerLiteral, value: int):
        self.assertTrue(isinstance(il, IntegerLiteral))
        self.assertEqual(value, il.value)
        self.assertEqual(f'{value}', il.token_literal())

    def identifier_test_helper(self, exp: Identifier, value: str):
        self.assertTrue(isinstance(exp, Identifier))
        self.assertEqual(value, exp.value)
        self.assertEqual(value, exp.token_literal())

    def boolean_literal_test_helper(self, bl: Boolean, value: bool):
        self.assertTrue(isinstance(bl, Boolean))
        self.assertEqual(value, bl.value)
        self.assertEqual(f'{value}'.lower(), bl.token_literal())
