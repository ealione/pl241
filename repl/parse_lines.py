# -*- encoding: utf-8 -*-
from _exceptions import IncompleteStatement
from ast import Program
from lexer import Lexer
from parser import Parser


# TODO: This is deprecated for now


class ReplParser(object):
    def __init__(self):
        self._current_input = None
        self.line = 1
        self._resume = ''
        self.program = Program()
        self.parser = Parser()
        self._error = ''

    @property
    def resume(self):
        return self._resume != ''

    def clean(self):
        self._resume = ''
        self._error = ''
        self.line = 1
        self.program = Program()
        self.parser = Parser()

    def parse(self, source: str) -> None:
        source = self._resume + source
        lex = Lexer(source)
        self.parser.new_lexer(lex)

        try:
            program = self.parser.parse_program()
            if len(self.parser.errors):
                self._error = f"{self.parser.errors[0]} Line {self.line}"
            self.program += program
            self._resume = ''
        except IncompleteStatement:
            self._resume = source + ' '
        finally:
            self.line += 1
