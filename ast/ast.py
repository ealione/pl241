# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from textwrap import indent, shorten
from typing import List, Dict, final, Union, Tuple

from tokens import Token


@dataclass
class Node:
    def token_literal(self) -> str:
        raise NotImplementedError

    def __str__(self) -> str:
        raise NotImplementedError


@dataclass
class Statement(Node):
    def statement_node(self):
        raise NotImplementedError


@dataclass
class Expression(Node):
    def expression_node(self):
        raise NotImplementedError


@dataclass
class Program(Node):
    statements: List[Statement] = field(default_factory=list)

    def _wrong_type_error(self, o) -> None:
        raise ValueError(f"Provided type [{type(o)}] is not of type `Statement`!")

    def token_literal(self) -> str:
        if len(self.statements) > 0:
            return self.statements[0].token_literal()
        else:
            return ""

    def __add__(self, other: Program) -> Program:
        if isinstance(other, Program):
            self.statements.extend(other.statements)
            return self
        else:
            raise NotImplemented

    def __str__(self) -> str:
        statement_list = "".join(f"{str(s)}" for s in self.statements)
        return statement_list

    def __len__(self) -> int:
        return len(self.statements)

    def __setitem__(self, i: int, val: Statement) -> None:
        if isinstance(val, Statement):
            self.statements[i] = val
        else:
            self._wrong_type_error(val)

    def __getitem__(self, i: int) -> Statement:
        return self.statements[i]

    def __delitem__(self, i: int) -> None:
        del self.statements[i]

    def insert(self, i: int, val: Statement) -> None:
        if isinstance(val, Statement) or isinstance(val, Expression):
            self.statements.insert(i, val)
        else:
            self._wrong_type_error(val)

    def append(self, val: Statement) -> None:
        self.insert(len(self.statements), val)

    def __key(self) -> Tuple[List[Statement], str]:
        return self.statements, self.token_literal()

    def __hash__(self) -> int:
        return hash(self.__key())

    def __eq__(self, other: Program) -> bool:
        if isinstance(other, Program):
            return self.__key() == other.__key()
        return NotImplemented

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


# Expressions
@dataclass
class Identifier(Expression):
    token: Token
    value: str

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{self.value}'

    def __eq__(self, other: Union[str, Identifier]):
        if isinstance(other, str):
            return self.value == other
        elif isinstance(other, Identifier):
            return self.__key() == other.__key()
        else:
            raise NotImplemented

    def __key(self) -> Tuple[Token, str]:
        return self.token, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class Boolean(Expression):
    token: Token
    value: bool = field(default=False)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{str(self.value).lower()}'

    def __bool__(self) -> bool:
        return self.value

    def __eq__(self, other: Union[bool, Boolean]):
        if isinstance(other, bool):
            return self.value == other
        elif isinstance(other, Boolean):
            return self.value == other.value
        else:
            raise NotImplemented

    def __key(self) -> Tuple[Token, bool]:
        return self.token, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class IntegerLiteral(Expression):
    token: Token
    value: int = field(default=0, compare=True)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{self.value}'

    def __eq__(self, other: Union[int, str, IntegerLiteral]) -> bool:
        if isinstance(other, int):
            return self.value == other
        if isinstance(other, str):
            return self.token_literal() == other
        elif isinstance(other, IntegerLiteral):
            return self.value == other.value
        else:
            raise NotImplemented

    def __ne__(self, other: Union[int, IntegerLiteral]) -> bool:
        if isinstance(other, int):
            return self.value != other
        elif isinstance(other, IntegerLiteral):
            return self.value != other.value
        else:
            raise NotImplemented

    def __gt__(self, other: Union[int, IntegerLiteral]) -> bool:
        if isinstance(other, int):
            return self.value > other
        elif isinstance(other, IntegerLiteral):
            return self.value > other.value
        else:
            raise NotImplemented

    def __ge__(self, other: Union[int, IntegerLiteral]) -> bool:
        if isinstance(other, int):
            return self.value >= other
        elif isinstance(other, IntegerLiteral):
            return self.value >= other.value
        else:
            raise NotImplemented

    def __lt__(self, other: Union[int, IntegerLiteral]) -> bool:
        if isinstance(other, int):
            return self.value < other
        elif isinstance(other, IntegerLiteral):
            return self.value < other.value
        else:
            raise NotImplemented

    def __le__(self, other: Union[int, IntegerLiteral]) -> bool:
        if isinstance(other, int):
            return self.value <= other
        elif isinstance(other, IntegerLiteral):
            return self.value <= other.value
        else:
            raise NotImplemented

    def __add__(self, other: Union[int, IntegerLiteral]) -> int:
        if isinstance(other, int):
            return self.value + other
        elif isinstance(other, IntegerLiteral):
            return self.value + other.value
        else:
            raise NotImplemented

    def __mul__(self, other: Union[int, IntegerLiteral]) -> int:
        if isinstance(other, int):
            return self.value * other
        elif isinstance(other, IntegerLiteral):
            return self.value * other.value
        else:
            raise NotImplemented

    def __sub__(self, other: Union[int, IntegerLiteral]) -> int:
        if isinstance(other, int):
            return self.value - other
        elif isinstance(other, IntegerLiteral):
            return self.value - other.value
        else:
            raise NotImplemented

    def __div__(self, other: Union[int, IntegerLiteral]) -> int:
        if isinstance(other, int):
            return self.value // other
        elif isinstance(other, IntegerLiteral):
            return self.value // other.value
        else:
            raise NotImplemented

    def __key(self) -> Tuple[Token, int]:
        return self.token, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class PrefixExpression(Expression):
    token: Token
    operator: str
    right: Expression = field(default_factory=Expression)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'({self.operator}{str(self.right)})'

    def __key(self) -> Tuple[Token, str, Expression]:
        return self.token, self.operator, self.right

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class InfixExpression(Expression):
    token: Token
    left: Expression
    operator: str
    right: Expression = field(default_factory=Expression)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'({str(self.left)} {self.operator} {str(self.right)})'

    def __key(self) -> Tuple[Token, Expression, str, Expression]:
        return self.token, self.left, self.operator, self.right

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class IfExpression(Expression):
    token: Token
    condition: Expression = field(default_factory=Expression)
    consequence: BlockStatement = field(default_factory=Expression)
    alternative: BlockStatement = field(default_factory=Expression)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        if_part = indent(str(self.consequence), prefix="  ")
        if_part = f'if {str(self.condition)} {{\n{if_part}'
        else_part = '\n}'
        if type(self.alternative) is not Expression:
            else_part = indent(str(self.alternative), prefix="  ")
            else_part = f'\n}} else {{\n{else_part}\n}}'
        return f'{if_part}{else_part}'

    def __key(self) -> Tuple[Token, Expression, BlockStatement, BlockStatement]:
        return self.token, self.condition, self.consequence, self.alternative

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class WhileExpression(Expression):
    token: Token
    condition: Expression = field(default_factory=Expression)
    body: BlockStatement = field(default_factory=Expression)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        body = indent(str(self.body), prefix="  ")
        body = f'while{str(self.condition)} {{\n{body}\n}}'
        return f'{body}'

    def __key(self) -> Tuple[Token, Expression, BlockStatement]:
        return self.token, self.condition, self.body

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class FunctionLiteral(Expression):
    token: Token
    body: BlockStatement = field(default_factory=Expression)
    declarations: BlockStatement = field(default_factory=Expression)
    parameters: List[Identifier] = field(default_factory=list)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        parameter_list = ", ".join(f"{str(p)}" for p in self.parameters)
        parameter_list = shorten(parameter_list, width=70)
        return f'{self.token_literal()}({parameter_list}) \n[\n{self.declarations}\n] \n{{\n{str(self.body)}\n}};'

    def __key(self) -> Tuple[Token, List[Identifier], BlockStatement, BlockStatement]:
        return self.token, self.parameters, self.declarations, self.body

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class MainLiteral(Expression):
    token: Token
    body: BlockStatement = field(default_factory=Expression)
    declarations: BlockStatement = field(default_factory=Expression)
    functions: BlockStatement = field(default_factory=Expression)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{self.token_literal()} \n[\n{self.declarations}\n] \n[\n{self.functions}\n] \n{{\n{str(self.body)}\n}};'

    def __key(self) -> Tuple[Token, BlockStatement, BlockStatement, BlockStatement]:
        return self.token, self.functions, self.declarations, self.body

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class ProcedureLiteral(Expression):
    token: Token
    body: BlockStatement = field(default_factory=Expression)
    declarations: BlockStatement = field(default_factory=Expression)
    parameters: List[Identifier] = field(default_factory=list)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        parameter_list = ", ".join(f"{str(p)}" for p in self.parameters)
        parameter_list = shorten(parameter_list, width=70)
        return f'{self.token_literal()}({parameter_list}) \n[\n{self.declarations}\n] \n{{\n{str(self.body)}\n}};'

    def __key(self) -> Tuple[Token, List[Identifier], BlockStatement, BlockStatement]:
        return self.token, self.parameters, self.declarations, self.body

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class StringLiteral(Expression):
    token: Token
    value: str = field(default='', compare=True)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return shorten(f'{self.value}', width=70)

    def __eq__(self, other):
        if isinstance(other, str):
            return self.value == other
        elif isinstance(other, StringLiteral):
            return self.value == other.value
        else:
            raise NotImplemented

    def __key(self) -> Tuple[Token, str]:
        return self.token, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class ArrayLiteral(Expression):
    token: Token
    elements: List[Expression] = field(default_factory=list)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        element_list = ", ".join(f"{str(e)}" for e in self.elements)
        element_list = shorten(element_list, width=70)
        return f'[{element_list}]'

    def __len__(self) -> int:
        return len(self.elements)

    def __contains__(self, item: Expression):
        return item in self.elements

    def __key(self) -> Tuple[Token, List[Expression]]:
        return self.token, self.elements

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class IndexExpression(Expression):
    token: Token
    left: Expression
    index: Expression = field(default_factory=Expression)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'({str(self.left)}[{str(self.index)}])'

    def __key(self) -> Tuple[Token, Expression, Expression]:
        return self.token, self.left, self.index

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class HashLiteral(Expression):
    token: Token
    pairs: Dict[Expression, Expression] = field(default_factory=dict)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        hash_pairs = "\n".join(f"{key}: {value}" for key, value in self.pairs.items())
        hash_pairs = shorten(hash_pairs, width=70)
        return f'{{{hash_pairs}}}'

    def __key(self) -> Tuple[Token, Dict[Expression, Expression]]:
        return self.token, self.pairs

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"

    def __setitem__(self, key, item):
        self.pairs[key] = item

    def __getitem__(self, key):
        return self.pairs[key]

    def __len__(self):
        return len(self.pairs)

    def __delitem__(self, key):
        del self.pairs[key]

    def clear(self):
        return self.pairs.clear()

    def copy(self):
        return self.pairs.copy()

    def has_key(self, k):
        return k in self.pairs

    def update(self, *args, **kwargs):
        return self.pairs.update(*args, **kwargs)

    def keys(self):
        return self.pairs.keys()

    def values(self):
        return self.pairs.values()


@dataclass
class CallExpression(Expression):
    token: Token
    function: Identifier = field(default=str)
    arguments: List[Identifier] = field(default_factory=list)

    def expression_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        argument_list = ", ".join(f"{str(a)}" for a in self.arguments)
        argument_list = shorten(argument_list, width=70)
        return f'call {str(self.function)}({argument_list})'

    def __key(self) -> Tuple[Token, Identifier, List[Identifier]]:
        return self.token, self.function, self.arguments

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


# Statements
@dataclass
class VariableDeclarationStatement(Statement):
    token: Token
    variables: List[Identifier] = field(default_factory=list)

    @final
    def statement_node(self):
        pass

    @final
    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        variables_list = ", ".join(f"{str(e)}" for e in self.variables)
        variables_list = shorten(variables_list, width=70)
        return f'{self.token.literal} [{variables_list}]'

    def __len__(self) -> int:
        return len(self.variables)

    def __getitem__(self, i: int) -> Identifier:
        return self.variables[i]

    def __key(self) -> Tuple[Token, List[Identifier]]:
        return self.token, self.variables

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class ArrayDeclarationStatement(Statement):
    token: Token
    size: List[Expression] = field(default=None)
    arrays: List[Identifier] = field(default_factory=list)

    @final
    def statement_node(self):
        pass

    @final
    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        array_size = ", ".join(f"{s}" for s in self.size)
        array_names_list = ", ".join(f"{str(e)}[{array_size}]" for e in self.arrays)
        variables_list = shorten(array_names_list, width=70)
        return f'{self.token.literal} [{variables_list}]'

    def __len__(self) -> int:
        return len(self.arrays)

    def __getitem__(self, i: int) -> Identifier:
        return self.arrays[i]

    def __key(self) -> Tuple[Token, List[Expression], List[Identifier]]:
        return self.token, self.size, self.arrays

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class BlockStatement(Statement):
    token: Token
    statements: List[Statement] = field(default_factory=list)

    def statement_node(self):
        pass

    def token_literal(self) -> str:
        return self.token.literal

    def __bool__(self) -> bool:
        return len(self.statements) != 0

    def __str__(self) -> str:
        statement_list = "\n".join(f"{str(s)}" for s in self.statements)
        return indent(statement_list, prefix='\t')

    def __len__(self) -> int:
        return len(self.statements)

    def __getitem__(self, i: int) -> Statement:
        return self.statements[i]

    def __delitem__(self, i: int) -> None:
        del self.statements[i]

    def __setitem__(self, i: int, val: Statement) -> None:
        if isinstance(val, Statement):
            self.statements[i] = val
        else:
            raise ValueError()

    def insert(self, i: int, val: Statement) -> None:
        if isinstance(val, Statement):
            self.statements.insert(i, val)
        else:
            raise ValueError()

    def append(self, val: Statement) -> None:
        self.insert(len(self.statements), val)

    def __key(self) -> Tuple[Token, List[Statement]]:
        return self.token, self.statements

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class LetStatement(Statement):
    token: Token
    name: Identifier = field(default_factory=Expression)
    value: Expression = field(default_factory=Expression)

    @final
    def statement_node(self):
        pass

    @final
    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{self.token_literal()} {str(self.name)} = {str(self.value)};'

    def __key(self) -> Tuple[Token, Identifier, Expression]:
        return self.token, self.name, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class CommentStatement(Statement):
    token: Token
    value: str

    @final
    def statement_node(self):
        pass

    @final
    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'// {str(self.value)}\n'

    def __key(self) -> Tuple[Token, str]:
        return self.token, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class ReturnStatement(Statement):
    token: Token
    value: Expression = field(default_factory=Expression)

    @final
    def statement_node(self):
        pass

    @final
    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{self.token_literal()} {str(self.value)};'

    def __key(self) -> Tuple[Token, Expression]:
        return self.token, self.value

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class ExpressionStatement(Statement):
    token: Token
    expression: Expression = field(default_factory=Expression)

    @final
    def statement_node(self):
        pass

    @final
    def token_literal(self) -> str:
        return self.token.literal

    def __str__(self) -> str:
        return f'{str(self.expression)}'  # TODO: should I have a semicolon here?

    def __key(self) -> Tuple[Token, Expression]:
        return self.token, self.expression

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"
