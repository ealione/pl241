# -*- encoding: utf-8 -*-
from unittest import TestCase

from ast import *
from tokens import Tokens, Token


class TestAST(TestCase):
    def test_string(self):
        program = Program(
            statements=[
                LetStatement(
                    token=Token.from_value(Tokens.LET),
                    name=Identifier(
                        token=Token(type=Tokens.IDENT, literal='MyVar'),
                        value='MyVar'
                    ),
                    value=Identifier(
                        token=Token(type=Tokens.IDENT, literal='AnotherVar'),
                        value='AnotherVar'
                    )
                )
            ]
        )

        self.assertEqual("let MyVar = AnotherVar;", str(program))
