# -*- encoding: utf-8 -*-
from argparse import ArgumentParser

from _version import __version__
from graph import control_flow_graph
from repl import Repl
from utils import highlight


def parse_args():
    parser = ArgumentParser(description='GUARDIAN v%s' % __version__)
    command_group = parser.add_mutually_exclusive_group()
    command_group.add_argument('-f', '--file', type=str, help="parse the specified file")

    return parser.parse_args()


if __name__ == "__main__":
    args = (parse_args())
    if args.file:
        program, errors, bytecode, constants, c_errors = Repl.run_file(args.file)
        print(highlight(str(program)))
        for i, e in enumerate(errors):
            print(f"ERROR [{i}]: {str(e)}")
        print(bytecode)
        print(c_errors)
        control_flow_graph(bytecode)
    else:
        print("No Errors found.")
