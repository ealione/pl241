// Array declaration test
main
var x, y;
array [ 4 ][ 2 ] arr;
{
    let x <- call InputNum();
    let y <- 2 * x;
    let x <- 2 / 3
    let x <- y
    // let y <- call func(5)
    call OutputNum(y)

    var i, j;
    let i <- 2;
    let j <- 1;
    if (i < j) then
     let i <- j
    else
     let j <- i
    fi

    let arr[ i ][ j ] <- j;
}.
