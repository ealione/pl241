# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from typing import Union, Tuple

from .tokens import Tokens, token_type


@dataclass
class Token:
    type: Union[Tokens, int]
    literal: str

    @classmethod
    def from_value(cls: Token, value) -> Token:
        try:
            if type(value) is str:
                return cls._from_literal(value)
            elif type(value) is int:
                return cls._from_type(value)
        except ValueError:
            return Token(type=Tokens.IDENT, literal=value)

    @classmethod
    def _from_literal(cls: Token, literal: str) -> Token:
        if literal not in token_type:
            raise ValueError
        return Token(type=token_type[literal], literal=literal)

    @classmethod
    def _from_type(cls: Token, type: Tokens) -> Token:
        literal = next((c_literal for c_literal, c_type in token_type.items() if c_type == type), None)
        if not literal:
            raise ValueError
        return Token(type=type, literal=literal)

    def __str__(self) -> str:
        return f'{self.literal}'

    def __eq__(self, other: Union[str, int]) -> bool:
        if isinstance(other, str):
            return self.literal == other
        elif isinstance(other, int):
            return self.type == other

    def __key(self) -> Tuple[Tokens, str]:
        return self.type, self.literal

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"
