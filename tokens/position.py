# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field


@dataclass
class Position:
    read: int = field(default=0)
    current: int = field(default=0)
    line: int = field(default=0)
    column: int = field(default=0)

    def advance(self) -> None:
        self.current = self.read
        self.read += 1
        self.column += 1

    def new_line(self) -> None:
        self.line += 1
        self.column = 0

    def __copy__(self) -> Position:
        cls = self.__class__
        result = cls.__new__(cls)
        result.__dict__.update(self.__dict__)
        return result
