# -*- encoding: utf-8 -*-
from dataclasses import dataclass
from typing import Final, Dict


@dataclass
class Tokens:
    # TODO: instead of a class do it like in symbol_table scopes
    ILLEGAL: Final = 0
    EOF: Final = 1

    # identifiers/literals
    LET: Final = 10
    VAR: Final = 11
    ARRAY: Final = 12
    STRING: Final = 13
    INT: Final = 14
    IDENT: Final = 15

    # operators
    PLUS: Final = 20
    MINUS: Final = 21
    BANG: Final = 22
    ASTERISK: Final = 23
    DIV: Final = 24

    # assignment
    ASSIGN: Final = 30

    # comments
    DOUBLESLASH: Final = 40
    HASH: Final = 41

    # comparison operators
    LT: Final = 50
    GT: Final = 51
    LEQT: Final = 52
    GEQT: Final = 53
    EQ: Final = 54
    NOT_EQ: Final = 55

    # delimiters
    PERIOD: Final = 60
    COMMA: Final = 61
    SEMICOLON: Final = 62
    COLON: Final = 63

    # grouping
    LPAREN: Final = 70
    RPAREN: Final = 71
    LBRACKET: Final = 72
    RBRACKET: Final = 73
    LBRACE: Final = 74
    RBRACE: Final = 75

    # keywords
    FUNCTION: Final = 80
    PROCEDURE: Final = 81
    CALL: Final = 82
    WHILE: Final = 83
    IF: Final = 84
    ELSE: Final = 85
    FI: Final = 86
    THEN: Final = 87
    DO: Final = 88
    OD: Final = 89
    MAIN: Final = 90
    RETURN: Final = 91
    TRUE: Final = 92
    FALSE: Final = 93


token_type: Final[Dict[str, Tokens]] = {
    "ILLEGAL": Tokens.ILLEGAL,
    "EOF": Tokens.EOF,
    "let": Tokens.LET,
    "var": Tokens.VAR,
    "array": Tokens.ARRAY,
    "string": Tokens.STRING,
    "int": Tokens.INT,
    "identifier": Tokens.IDENT,
    "+": Tokens.PLUS,
    "-": Tokens.MINUS,
    "!": Tokens.BANG,
    "*": Tokens.ASTERISK,
    "/": Tokens.DIV,
    "<-": Tokens.ASSIGN,
    "//": Tokens.DOUBLESLASH,
    "#": Tokens.HASH,
    "<": Tokens.LT,
    ">": Tokens.GT,
    "<=": Tokens.LEQT,
    ">=": Tokens.GEQT,
    "==": Tokens.EQ,
    "!=": Tokens.NOT_EQ,
    ".": Tokens.PERIOD,
    ",": Tokens.COMMA,
    ";": Tokens.SEMICOLON,
    ":": Tokens.COLON,
    "(": Tokens.LPAREN,
    ")": Tokens.RPAREN,
    "[": Tokens.LBRACKET,
    "]": Tokens.RBRACKET,
    "{": Tokens.LBRACE,
    "}": Tokens.RBRACE,
    "function": Tokens.FUNCTION,
    "procedure": Tokens.PROCEDURE,
    "call": Tokens.CALL,
    "while": Tokens.WHILE,
    "if": Tokens.IF,
    "else": Tokens.ELSE,
    "fi": Tokens.FI,
    "then": Tokens.THEN,
    "do": Tokens.DO,
    "od": Tokens.OD,
    "main": Tokens.MAIN,
    "return": Tokens.RETURN,
    "true": Tokens.TRUE,
    "false": Tokens.FALSE
}
