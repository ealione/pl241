# -*- encoding: utf-8 -*-
from __future__ import annotations

from typing import ByteString, List, Tuple

from .definitions import Definition, OpCode, definitions


class Instruction:
    def __init__(self, opcode: OpCode, *operands: int):
        self._opcode = opcode
        self._defn = self._lookup()
        self._ins = self._make(operands)
        self._operands = self._read_operands()

    @property
    def type(self) -> OpCode:
        return self._opcode

    @property
    def operands(self) -> List[int]:
        return self._operands

    @property
    def offset(self) -> int:
        return sum(self._defn.operand_widths)

    def __bytes__(self) -> ByteString:
        return self._ins

    def __str__(self) -> str:
        op_list = "".join(f"{op: d}" for op in self._operands)
        return f'{self._defn.name}{op_list}'

    def __len__(self) -> int:
        return len(self._ins)

    def __key(self) -> Tuple[OpCode, Definition, ByteString, str]:
        return self._opcode, self._defn, self._ins, str(self.operands)

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"

    def __eq__(self, other: Instruction):
        return self._opcode == other._opcode and self.operands == other.operands and self.type == other.type

    @property
    def uint8_ops(self):
        ops = [int.from_bytes(bytes(self._ins)[i + 1: i + 2], byteorder='big')
               for i in range(sum(self._defn.operand_widths))]
        return [self._opcode, *ops]

    def _lookup(self) -> Definition:
        defn = definitions.get(self._opcode, None)
        if defn is not None:
            return defn
        else:
            raise IndexError

    def _make(self, operands) -> ByteString:
        instruction = bytes()
        instruction += self._opcode.to_bytes(1, byteorder='big')

        for i, o in enumerate(operands):
            width = self._defn.operand_widths[i]
            instruction += o.to_bytes(width, byteorder='big')

        return instruction

    def _read_operands(self) -> [int, List[int]]:
        operands = []
        start = 0
        for w in self._defn.operand_widths:
            end = start + w
            operands.append(int.from_bytes(self._ins[1:][start:end], byteorder='big'))
            start = end

        if len(operands) != len(self._defn.operand_widths):
            raise AssertionError

        return operands
