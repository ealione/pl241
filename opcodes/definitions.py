# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from typing import List, Dict, Final, Tuple

OpCode = int

OpMinus: Final[OpCode] = 0
OpAdd: Final[OpCode] = 1
OpSub: Final[OpCode] = 2
OpMul: Final[OpCode] = 3
OpDiv: Final[OpCode] = 4
OpCmp: Final[OpCode] = 5
OpAdda: Final[OpCode] = 6
OpLoad: Final[OpCode] = 6
OpStore: Final[OpCode] = 8
OpMove: Final[OpCode] = 9
OpPhi: Final[OpCode] = 10
OpEnd: Final[OpCode] = 11
OpBranch: Final[OpCode] = 12
OpNotEqual: Final[OpCode] = 13
OpEqual: Final[OpCode] = 14
OpLessEqual: Final[OpCode] = 15
OpLessThan: Final[OpCode] = 16
OpGreaterEqual: Final[OpCode] = 17
OpGreaterThan: Final[OpCode] = 18
OpRead: Final[OpCode] = 19
OpWrite: Final[OpCode] = 20
OpWriteNL: Final[OpCode] = 21
OpConstant: Final[OpCode] = 22
OpReturn: Final[OpCode] = 23


@dataclass
class Definition:
    name: str
    operand_widths: List[int] = field(default_factory=list)

    def __key(self) -> Tuple[str, str]:
        return self.name, str(self.operand_widths)

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


definitions: Final[Dict[OpCode, Definition]] = {
    OpMinus: Definition(name="Neg", operand_widths=[2]),
    OpAdd: Definition(name="Add", operand_widths=[2, 2]),
    OpSub: Definition(name="Sub", operand_widths=[2, 2]),
    OpMul: Definition(name="Mul", operand_widths=[2, 2]),
    OpDiv: Definition(name="Div", operand_widths=[2, 2]),
    OpCmp: Definition(name="Cmp", operand_widths=[2, 2]),
    OpAdda: Definition(name="Adda", operand_widths=[2, 2]),
    OpLoad: Definition(name="Load", operand_widths=[2]),
    OpStore: Definition(name="Store", operand_widths=[2, 2]),
    OpMove: Definition(name="Move", operand_widths=[2, 2]),
    OpPhi: Definition(name="Phi", operand_widths=[2, 2]),
    OpEnd: Definition(name="End", operand_widths=[]),
    OpBranch: Definition(name="Bra", operand_widths=[2]),
    OpNotEqual: Definition(name="Bne", operand_widths=[2, 2]),
    OpEqual: Definition(name="Beq", operand_widths=[2, 2]),
    OpLessEqual: Definition(name="Ble", operand_widths=[2, 2]),
    OpLessThan: Definition(name="Blt", operand_widths=[2, 2]),
    OpGreaterEqual: Definition(name="Bge", operand_widths=[2, 2]),
    OpGreaterThan: Definition(name="Bgt", operand_widths=[2, 2]),
    OpRead: Definition(name="Read", operand_widths=[2]),
    OpWrite: Definition(name="Write", operand_widths=[2]),
    OpWriteNL: Definition(name="WriteNL", operand_widths=[]),
    OpConstant: Definition(name="Constant", operand_widths=[2]),
    OpReturn: Definition(name="Return", operand_widths=[2])
}
