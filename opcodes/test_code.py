# -*- encoding: utf-8 -*-
from unittest import TestCase

from opcodes import *


class TestCode(TestCase):
    def test_make(self):
        tests = [
            (OpMinus, [65534], [OpMinus, 255, 254]),
            (OpAdd, [1, 2], [OpAdd, 0, 1, 0, 2]),
            (OpEnd, [], [OpEnd]),
            (OpDiv, [65534, 255], [OpDiv, 255, 254, 0, 255])
        ]

        for test in tests:
            instruction = Instruction(test[0], *test[1])

            self.assertEqual(len(test[2]), len(instruction))

            for i, ex in enumerate(test[2]):
                self.assertEqual(ex, instruction.uint8_ops[i])

    def test_instruction_string(self):
        instructions = Instructions([
            Instruction(OpAdd),
            Instruction(OpLoad, 1),
            Instruction(OpConstant, 2),
            Instruction(OpConstant, 65535),
            Instruction(OpMove, 65535, 255)
        ])

        expected = "0000 Add 0 0\n" \
                   "0005 Load 1\n" \
                   "0008 Constant 2\n" \
                   "0011 Constant 65535\n" \
                   "0014 Move 65535 255"

        self.assertEqual(expected, str(instructions))

    def test_read_operands(self):
        tests = [
            (OpConstant, [65535], 2),
            (OpLoad, [255], 2),
            (OpMove, [65535, 255], 4)
        ]

        for test in tests:
            instruction = Instruction(test[0], *test[1])

            self.assertEqual(test[1], instruction.operands)
            self.assertEqual(test[2], len(instruction.uint8_ops) - 1)
