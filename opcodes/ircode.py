# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from typing import NewType, Final, List, Tuple

from compiler.symbol_table import Symbol
from .bcode import OpCode, definitions, Definition

OperandType = NewType('OperandType', str)

CONSTANT_OPERAND: Final[OperandType] = OperandType("CONSTANT")
ADDRESS_OPERAND: Final[OperandType] = OperandType("ADDRESS")
LITERAL_OPERAND: Final[OperandType] = OperandType("LITERAL")
SYMBOL_OPERAND: Final[OperandType] = OperandType("SYMBOL")


@dataclass
class Operand:
    type: OperandType

    def __str__(self) -> str:
        return NotImplemented


@dataclass
class ConstantOperand(Operand):
    value: int = field(default=0)
    type: OperandType = field(default=CONSTANT_OPERAND)

    def __str__(self) -> str:
        return f"#{self.value}"

    def __hash__(self) -> int:
        return hash((self.type, self.value))


@dataclass
class AddressOperand(Operand):
    value: int = field(default=0)
    type: OperandType = field(default=ADDRESS_OPERAND)

    def __str__(self) -> str:
        return f"({self.value})"

    def __hash__(self) -> int:
        return hash((self.type, self.value))


@dataclass
class LiteralOperand(Operand):
    value: str = field(default_factory=None)
    index: int = field(default=None)
    type: OperandType = field(default=LITERAL_OPERAND)

    def __str__(self) -> str:
        if self.index == 0:
            return f"[{self.value}]"
        else:
            return f"[{self.value}_{self.index}]"

    def __hash__(self) -> int:
        return hash((self.type, self.value))


@dataclass
class SymbolOperand(Operand):
    value: int = field(default=None)
    symbol: Symbol = field(default_factory=Symbol)
    type: OperandType = field(default=SYMBOL_OPERAND)

    def __str__(self) -> str:
        return f"[{str(self.symbol)}${self.value}]"

    def __hash__(self) -> int:
        return hash((self.type, self.value))


class IRInstruction:
    def __init__(self, opcode: OpCode, *operands: Operand):
        self.opcode = opcode
        self._operands = self._read_operands(operands)

    def _lookup(self) -> Definition:
        defn = definitions.get(self.opcode, None)
        if defn is not None:
            return defn
        else:
            raise IndexError

    @property
    def operands(self) -> List[Operand]:
        return self._operands

    @property
    def type(self) -> OpCode:
        return self.opcode

    def __str__(self) -> str:
        operands = " ".join(f"{str(op):>15}" for op in self.operands)
        return f"{self._lookup().name:10} {operands}"

    def __len__(self) -> int:
        return len(self.operands)

    @property
    def offset(self) -> int:
        return 0

    @staticmethod
    def _read_operands(operands: Tuple[Operand]) -> List[Operand]:
        return list(operands)

    def __key(self) -> Tuple[OpCode, str]:
        return self.opcode, str(self.operands)

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"

    def __eq__(self, other: IRInstruction):
        return self.opcode == other.opcode and self.operands == other.operands
