# -*- encoding: utf-8 -*-
from .bcode import *
from .definitions import *
from .instructions import *
from .ircode import *

__all__ = [
    "OpCode",
    "Instruction",
    "Instructions",
    "OpMinus",
    "OpAdd",
    "OpSub",
    "OpMul",
    "OpDiv",
    "OpCmp",
    "OpAdda",
    "OpLoad",
    "OpStore",
    "OpMove",
    "OpPhi",
    "OpEnd",
    "OpBranch",
    "OpNotEqual",
    "OpEqual",
    "OpLessEqual",
    "OpLessThan",
    "OpGreaterEqual",
    "OpGreaterThan",
    "OpRead",
    "OpWrite",
    "OpWriteNL",
    "IRInstruction",
    "ConstantOperand",
    "AddressOperand",
    "LiteralOperand",
    "SymbolOperand",
    "Operand",
    "OpConstant",
    "OpReturn"
]
