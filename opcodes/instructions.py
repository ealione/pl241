# -*- encoding: utf-8 -*-
from __future__ import annotations

from collections import MutableSequence
from dataclasses import dataclass, field
from typing import List, Any, Union

from .bcode import Instruction
from .ircode import IRInstruction


@dataclass
class Instructions(MutableSequence):
    _instructions: List[Union[Instruction, IRInstruction]] = field(default_factory=list)
    offset_start: int = field(default=0)

    def __add__(self, other):
        if isinstance(other, Instructions):
            self._instructions.extend(other._instructions)
        return self

    def extend(self, iterable: Instructions) -> None:
        self._instructions.extend(iterable._instructions)

    def __len__(self) -> int:
        return len(self._instructions)

    def __getitem__(self, i: int) -> Any[Instruction, List[Instruction]]:
        return self._instructions[i]

    def __delitem__(self, i: int) -> None:
        del self._instructions[i]

    def __setitem__(self, i: int, val: Instruction) -> None:
        self._instructions[i] = val

    def insert(self, i: int, val: Instruction) -> None:
        self._instructions.insert(i, val)

    def append(self, val: Instruction) -> None:
        self.insert(len(self._instructions), val)

    def __str__(self) -> str:
        out = ""
        offset = self.offset_start
        for ins in self._instructions:
            out += f"{offset:04d} {str(ins)}\n"
            offset += ins.offset + 1
        return out[:-1]

    def __key(self) -> str:
        return str(self._instructions)

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"
