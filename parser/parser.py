# -*- encoding: utf-8 -*-
from __future__ import annotations

from typing import List, Callable, Dict, Any, Final, Optional, SupportsInt, Union

from _exceptions import InvalidSyntaxError
from ast import Program, LetStatement, ReturnStatement, ExpressionStatement, Identifier, Expression, Statement, \
    IntegerLiteral, StringLiteral, PrefixExpression, InfixExpression, Boolean, IfExpression, BlockStatement, \
    FunctionLiteral, CallExpression, ArrayLiteral, IndexExpression, HashLiteral, ProcedureLiteral, WhileExpression, \
    VariableDeclarationStatement, ArrayDeclarationStatement, CommentStatement, MainLiteral
from lexer import Lexer
from tokens import Token, Tokens

PrefixParseFunction = Callable[[], Union[Expression, Statement]]
InfixParseFunction = Callable[[Expression], Union[Expression, Statement]]

ExpressionPrecedence = SupportsInt

LOWEST: Final[ExpressionPrecedence] = 1
EQUALS: Final[ExpressionPrecedence] = 2
LESSGREATER: Final[ExpressionPrecedence] = 3
SUM: Final[ExpressionPrecedence] = 4
PRODUCT: Final[ExpressionPrecedence] = 5
PREFIX: Final[ExpressionPrecedence] = 6
CALL: Final[ExpressionPrecedence] = 7
INDEX: Final[ExpressionPrecedence] = 8

precedence: Final[Dict[Tokens, ExpressionPrecedence]] = {
    Tokens.EQ: EQUALS,
    Tokens.NOT_EQ: EQUALS,
    Tokens.LT: LESSGREATER,
    Tokens.GT: LESSGREATER,
    Tokens.LEQT: LESSGREATER,
    Tokens.GEQT: LESSGREATER,
    Tokens.PLUS: SUM,
    Tokens.MINUS: SUM,
    Tokens.DIV: PRODUCT,
    Tokens.ASTERISK: PRODUCT,
    Tokens.LPAREN: CALL,
    Tokens.LBRACKET: INDEX
}


class Parser:
    def __init__(self, lex: Lexer = None):
        self.lex: Lexer = lex
        self._errors: List[InvalidSyntaxError] = []

        self.prefix_parse_functions: Dict[Tokens, PrefixParseFunction] = {}
        self.infix_parse_functions: Dict[Tokens, InfixParseFunction] = {}

        self.cur_token: Optional[Token] = None
        self.peek_token: Optional[Token] = None

        self.register_prefix_parse_functions()
        self.register_infix_parse_functions()
        self.next_token()
        self.next_token()

    def next_token(self) -> None:
        self.cur_token = self.peek_token
        self.peek_token = self.lex.get_next_token()

    def cur_token_is(self, t: Union[Tokens, List[Tokens]]) -> bool:
        if isinstance(t, list):
            return self.cur_token.type in t
        else:
            return self.cur_token.type == t

    def peek_token_is(self, t: Tokens) -> bool:
        return self.peek_token.type == t

    def expect_peek(self, t: Tokens) -> bool:
        if self.peek_token_is(t):
            self.next_token()
            return True
        else:
            self._peek_error(t)
            return False

    @property
    def errors(self) -> List[InvalidSyntaxError]:
        return self._errors

    def _peek_error(self, t: Tokens) -> None:
        err = InvalidSyntaxError(self.lex.last_position, self.lex.position,
                                 f"Expected next token to be '{str(Token.from_value(t))}', "
                                 f"got '{str(self.peek_token)}' instead!",
                                 self.lex.input, self.lex.filename)
        self._errors.append(err)

    def _no_prefix_parse_function_error(self, t: Tokens) -> None:
        err = InvalidSyntaxError(self.lex.last_position, self.lex.position,
                                 f"No prefix parse function found for '{str(Token.from_value(t))}'!",
                                 self.lex.input, self.lex.filename)
        self._errors.append(err)

    def parse_program(self) -> Program:
        program = Program()
        while not self.cur_token_is(Tokens.EOF):
            stmt = self.parse_statement()
            if stmt:
                program.append(stmt)
            self.next_token()
        return program

    def parse_statement(self) -> Any[Statement, Expression]:
        if self.cur_token.type == Tokens.LET:
            return self.parse_let_statement()
        elif self.cur_token.type == Tokens.RETURN:
            return self.parse_return_statement()
        elif self.cur_token.type == Tokens.ARRAY:
            return self.parse_array_declaration_statement()
        elif self.cur_token.type == Tokens.VAR:
            return self.parse_variable_declaration_statement()
        else:
            return self.parse_expression_statement()

    def parse_let_statement(self) -> Optional[LetStatement]:
        stmt = LetStatement(self.cur_token)

        if not self.expect_peek(Tokens.IDENT):
            return None

        stmt.name = self.parse_expression_statement()

        if not self.expect_peek(Tokens.ASSIGN):
            return None

        self.next_token()

        stmt.value = self.parse_expression(LOWEST)

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return stmt

    def parse_call_expression(self) -> Optional[CallExpression]:
        # TODO: Parse an expression instead of an identifier as a function name
        stmt = CallExpression(self.cur_token)

        if not self.expect_peek(Tokens.IDENT):
            return None

        stmt.function = self.parse_identifier()

        if not self.peek_token_is(Tokens.LPAREN):
            return stmt
        self.next_token()

        stmt.arguments = self.parse_expression_list(Tokens.RPAREN)

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return stmt

    def parse_comment_statement(self) -> Optional[CommentStatement]:
        stmt = CommentStatement(token=self.cur_token, value=self.cur_token.literal)

        return stmt

    def parse_return_statement(self) -> Optional[ReturnStatement]:
        stmt = ReturnStatement(token=self.cur_token)

        self.next_token()

        stmt.value = self.parse_expression(LOWEST)

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return stmt

    def parse_variable_declaration_statement(self) -> Optional[VariableDeclarationStatement]:
        stmt = VariableDeclarationStatement(token=self.cur_token)

        if not self.expect_peek(Tokens.IDENT):
            return None

        lst: List[Expression] = [self.parse_identifier()]
        while self.peek_token_is(Tokens.COMMA):
            self.next_token()
            self.next_token()
            lst.append(self.parse_identifier())

        stmt.variables = lst

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return stmt

    def parse_array_declaration_statement(self) -> Optional[ArrayDeclarationStatement]:
        stmt = ArrayDeclarationStatement(token=self.cur_token)

        size: List[Expression] = []
        while self.peek_token_is(Tokens.LBRACKET):
            self.next_token()
            self.next_token()
            size.append(self.parse_integer_literal())
            if not self.expect_peek(Tokens.RBRACKET):
                return None

        stmt.size = size

        if not self.expect_peek(Tokens.IDENT):
            return None

        lst: List[Expression] = [self.parse_identifier()]
        while self.peek_token_is(Tokens.COMMA):
            self.next_token()
            self.next_token()
            lst.append(self.parse_identifier())

        stmt.arrays = lst

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return stmt

    def parse_expression_statement(self) -> Optional[ExpressionStatement]:
        stmt = ExpressionStatement(token=self.cur_token)

        stmt.expression = self.parse_expression(LOWEST)

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return stmt

    def parse_expression(self, precedence: ExpressionPrecedence) -> Optional[Expression]:
        prefix = self.prefix_parse_functions.get(self.cur_token.type, None)

        if prefix is None:
            self._no_prefix_parse_function_error(self.cur_token.type)
            return None

        left_expr = prefix()

        while not self.peek_token_is(Tokens.SEMICOLON) and precedence < self.peek_precedence():
            infix = self.infix_parse_functions[self.peek_token.type]

            if infix is None:
                return left_expr

            self.next_token()
            left_expr = infix(left_expr)

        return left_expr

    def peek_precedence(self) -> ExpressionPrecedence:
        if self.peek_token.type in precedence:
            return precedence[self.peek_token.type]
        else:
            return LOWEST

    def cur_precedence(self) -> ExpressionPrecedence:
        if self.cur_token.type in precedence:
            return precedence[self.cur_token.type]
        else:
            return LOWEST

    def parse_identifier(self) -> Identifier:
        return Identifier(token=self.cur_token, value=self.cur_token.literal)

    def parse_integer_literal(self) -> IntegerLiteral:
        lit = IntegerLiteral(token=self.cur_token)
        # TODO: error check
        value = int(self.cur_token.literal)
        lit.value = value
        return lit

    def parse_string_literal(self) -> StringLiteral:
        return StringLiteral(token=self.cur_token, value=self.cur_token.literal)

    def parse_prefix_expression(self) -> PrefixExpression:
        epxression = PrefixExpression(token=self.cur_token, operator=self.cur_token.literal)
        self.next_token()
        epxression.right = self.parse_expression(PREFIX)

        return epxression

    def parse_infix_expression(self, left: Expression) -> InfixExpression:
        epxression = InfixExpression(
            token=self.cur_token,
            operator=self.cur_token.literal,
            left=left
        )
        precedence = self.cur_precedence()
        self.next_token()
        epxression.right = self.parse_expression(precedence)

        return epxression

    def parse_boolean(self) -> Boolean:
        return Boolean(token=self.cur_token, value=self.cur_token_is(Tokens.TRUE))

    def parse_grouped_expression(self) -> Optional[Expression]:
        self.next_token()
        expression = self.parse_expression(LOWEST)
        if not self.expect_peek(Tokens.RPAREN):
            return None
        return expression

    def parse_if_expression(self) -> Optional[IfExpression]:
        expression = IfExpression(token=self.cur_token)

        self.next_token()

        expression.condition = self.parse_expression(LOWEST)

        if not self.expect_peek(Tokens.THEN):
            return None

        expression.consequence = self.parse_block_statement([Tokens.ELSE, Tokens.FI])

        if not len(expression.consequence.statements):
            return None

        if self.cur_token_is(Tokens.ELSE):
            expression.alternative = self.parse_block_statement(Tokens.FI)

        if not self.cur_token_is(Tokens.FI):
            self._peek_error(Tokens.FI)
            return None

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return expression

    def parse_while_expression(self) -> Optional[WhileExpression]:
        # TODO: Add a filter for statements
        expression = WhileExpression(token=self.cur_token)

        self.next_token()

        expression.condition = self.parse_expression(LOWEST)

        if not self.expect_peek(Tokens.DO):
            return None

        expression.body = self.parse_block_statement(Tokens.OD)

        if not len(expression.body.statements):
            return None

        if not self.cur_token_is(Tokens.OD):
            self._peek_error(Tokens.OD)
            return None

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        return expression

    def parse_block_statement(self, end: List[Tokens]) -> BlockStatement:
        block = BlockStatement(token=self.cur_token)

        self.next_token()

        while not self.cur_token_is(end) and not self.cur_token_is(Tokens.EOF):
            stmt = self.parse_statement()
            if stmt is not None:
                block.statements.append(stmt)
            self.next_token()
        return block

    def parse_function_literal(self) -> Optional[FunctionLiteral]:
        if not self.expect_peek(Tokens.IDENT):
            return None

        lit = FunctionLiteral(token=self.cur_token)

        if not self.expect_peek(Tokens.LPAREN):
            return None

        lit.parameters = self.parse_function_parameters()

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        lit.declarations = self.parse_block_statement(Tokens.LBRACE)

        if not self.cur_token_is(Tokens.LBRACE):
            self._peek_error(Tokens.LBRACE)
            return None

        lit.body = self.parse_block_statement(Tokens.RBRACE)

        if not self.cur_token_is(Tokens.RBRACE):
            self._peek_error(Tokens.RBRACE)
            return None

        return lit

    def parse_procedure_literal(self) -> Optional[ProcedureLiteral]:
        if not self.expect_peek(Tokens.IDENT):
            return None

        lit = ProcedureLiteral(token=self.cur_token)

        if not self.expect_peek(Tokens.LPAREN):
            return None

        lit.parameters = self.parse_function_parameters()

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        lit.declarations = self.parse_block_statement(Tokens.LBRACE)

        if not self.cur_token_is(Tokens.LBRACE):
            self._peek_error(Tokens.LBRACE)
            return None

        lit.body = self.parse_block_statement(Tokens.RBRACE)

        if not self.cur_token_is(Tokens.RBRACE):
            self._peek_error(Tokens.RBRACE)
            return None

        return lit

    def parse_main_literal(self) -> Optional[MainLiteral]:
        lit = MainLiteral(token=self.cur_token)

        if self.peek_token_is(Tokens.SEMICOLON):
            self.next_token()

        header = self.parse_block_statement(Tokens.LBRACE)

        declarations, functions = BlockStatement(Tokens.VAR), BlockStatement(Tokens.FUNCTION)
        for d in header:
            (declarations if type(d) in [VariableDeclarationStatement,
                                         ArrayDeclarationStatement] else functions).append(d)

        lit.declarations, lit.functions = declarations, functions

        if not self.cur_token_is(Tokens.LBRACE):
            self._peek_error(Tokens.LBRACE)
            return None

        lit.body = self.parse_block_statement(Tokens.RBRACE)

        if not self.cur_token_is(Tokens.RBRACE):
            self._peek_error(Tokens.RBRACE)
            return None

        if self.peek_token_is(Tokens.PERIOD):
            self.next_token()

        return lit

    def parse_function_parameters(self) -> Optional[List[Identifier]]:
        identifiers: List[Identifier] = []

        if self.peek_token_is(Tokens.RPAREN):
            self.next_token()
            return identifiers

        self.next_token()

        ident = Identifier(token=self.cur_token, value=self.cur_token.literal)
        identifiers.append(ident)

        while self.peek_token_is(Tokens.COMMA):
            self.next_token()
            self.next_token()
            ident = Identifier(token=self.cur_token, value=self.cur_token.literal)
            identifiers.append(ident)

        if not self.expect_peek(Tokens.RPAREN):
            return None

        return identifiers

    def parse_expression_list(self, end: Tokens) -> Optional[List[Expression]]:
        lst: List[Expression] = []

        if self.peek_token_is(end):
            self.next_token()
            return lst

        self.next_token()
        lst.append(self.parse_expression(LOWEST))

        while self.peek_token_is(Tokens.COMMA):
            self.next_token()
            self.next_token()
            lst.append(self.parse_expression(LOWEST))

        if not self.expect_peek(end):
            return None

        return lst

    def parse_array_literal(self) -> ArrayLiteral:
        array = ArrayLiteral(token=self.cur_token)
        array.elements = self.parse_expression_list(Tokens.RBRACKET)
        return array

    def parse_index_expression(self, left: Expression) -> Optional[IndexExpression]:
        exp = IndexExpression(token=self.cur_token, left=left)

        self.next_token()

        exp.index = self.parse_expression(LOWEST)

        if not self.expect_peek(Tokens.RBRACKET):
            return None

        return exp

    def parse_hash_literal(self) -> Optional[HashLiteral]:
        hsh = HashLiteral(token=self.cur_token)

        while not self.peek_token_is(Tokens.RBRACE):
            self.next_token()
            key = self.parse_expression(LOWEST)

            if not self.expect_peek(Tokens.COLON):
                return None

            self.next_token()
            value = self.parse_expression(LOWEST)

            hsh.pairs[key] = value

            if not self.peek_token_is(Tokens.RBRACE) and not self.expect_peek(Tokens.COMMA):
                return None

        if not self.expect_peek(Tokens.RBRACE):
            return None

        return hsh

    def register_prefix_parse_functions(self) -> None:
        self.prefix_parse_functions[Tokens.IDENT] = self.parse_identifier
        self.prefix_parse_functions[Tokens.INT] = self.parse_integer_literal
        self.prefix_parse_functions[Tokens.STRING] = self.parse_string_literal
        self.prefix_parse_functions[Tokens.BANG] = self.parse_prefix_expression
        self.prefix_parse_functions[Tokens.MINUS] = self.parse_prefix_expression
        self.prefix_parse_functions[Tokens.TRUE] = self.parse_boolean
        self.prefix_parse_functions[Tokens.FALSE] = self.parse_boolean
        self.prefix_parse_functions[Tokens.LPAREN] = self.parse_grouped_expression
        self.prefix_parse_functions[Tokens.IF] = self.parse_if_expression
        self.prefix_parse_functions[Tokens.WHILE] = self.parse_while_expression
        self.prefix_parse_functions[Tokens.FUNCTION] = self.parse_function_literal
        self.prefix_parse_functions[Tokens.PROCEDURE] = self.parse_procedure_literal
        self.prefix_parse_functions[Tokens.LBRACKET] = self.parse_array_literal
        self.prefix_parse_functions[Tokens.LBRACE] = self.parse_hash_literal
        self.prefix_parse_functions[Tokens.DOUBLESLASH] = self.parse_comment_statement
        self.prefix_parse_functions[Tokens.HASH] = self.parse_comment_statement
        self.prefix_parse_functions[Tokens.MAIN] = self.parse_main_literal
        self.prefix_parse_functions[Tokens.CALL] = self.parse_call_expression

    def register_infix_parse_functions(self) -> None:
        self.infix_parse_functions[Tokens.PLUS] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.MINUS] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.DIV] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.ASTERISK] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.EQ] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.NOT_EQ] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.LEQT] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.GEQT] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.LT] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.GT] = self.parse_infix_expression
        self.infix_parse_functions[Tokens.LBRACKET] = self.parse_index_expression
