# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass
from textwrap import indent
from typing import Final


@dataclass
class ParserTracing:
    trace_level: int = 0

    trace_ident_placeholder: Final[str] = '  '

    def ident_level(self) -> str:
        return self.trace_ident_placeholder * (self.trace_level - 1)

    def trace_print(self, fs: str) -> None:
        msg = indent(fs, prefix=self.ident_level())
        print(msg)

    def inc_ident(self) -> None:
        self.trace_level += 1

    def dec_ident(self) -> None:
        self.trace_level -= 1

    def trace(self, msg: str) -> str:
        self.inc_ident()
        self.trace_print(f'BEGIN {msg}')
        return msg

    def untrace(self, msg: str) -> None:
        self.trace_print(f'END {msg}')
        self.dec_ident()
