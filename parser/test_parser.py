# -*- encoding: utf-8 -*-
from typing import Union
from unittest import TestCase

from ast import *
from lexer import Lexer
from parser import Parser
from tokens import Tokens


class TestParer(TestCase):

    def check_parser_errors(self, p):
        errors = p.errors
        self.assertEqual(len(errors), 0)

    def test_let_statements(self):
        tests = [
            ("let x <- 5;", "x", 5),
            ("let y <- true;", "y", True),
            ("let foobar <- y;", "foobar", "y")
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)

            self.assertEqual(len(program), 1)

            stmt = program[0]

            self.let_statement_test_helper(stmt, test[1])

            val = stmt.value
            self.literal_expression_test_helper(val, test[2])

    def test_let_call_statements(self):
        test = "let fn <- call Test();"

        l = Lexer(test)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)

        self.assertEqual(len(program), 1)

        stmt = program[0]

        self.assertTrue(isinstance(stmt, LetStatement))
        self.let_statement_test_helper(stmt, "fn")

        val = stmt.value
        self.assertTrue(isinstance(val, CallExpression))

    def test_let_complex_statements(self):
        tests = [
            ("let a[ i ][ j ] <- i;", "((a[i])[j])", "i"),
            ("let a[ 9 ][ 1 + call foo( ) ][ b * c ] <- 45;", "(((a[9])[(1 + call foo())])[(b * c)])", "45"),
            ("let d <- a[ 9 ][ call foo( ) + 1 ][ c * b ] + 2", "d", "((((a[9])[(call foo() + 1)])[(c * b)]) + 2)")
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)

            self.assertEqual(len(program), 1)

            stmt = program[0]

            name = stmt.name
            self.assertEqual(test[1], str(name))

            val = stmt.value
            self.assertEqual(test[2], str(val))

    def test_return_statements(self):
        tests = [
            ("return 5;", 5),
            ("return true;", True),
            ("return foobar;", "foobar"),
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            self.assertEqual(len(program), 1)

            stmt = program[0]
            self.assertTrue(isinstance(stmt, ReturnStatement))

            self.assertEqual('return', stmt.token_literal())
            self.assertEqual(Tokens.RETURN, stmt.token)

            self.literal_expression_test_helper(stmt.value, test[1])

    def test_variable_declarations(self):
        tests = [
            ("var x;", "var", ['x']),
            ("var x, y", "var", ['x', 'y'])
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)

            self.assertEqual(len(program), 1)

            stmt = program[0]
            self.assertTrue(isinstance(stmt, VariableDeclarationStatement))

            self.assertEqual(test[1], stmt.token_literal())

            vars = stmt.variables
            for i, tvar in enumerate(test[2]):
                self.literal_expression_test_helper(vars[i], tvar)

    def test_identifier_expression(self):
        input = "foobar;"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))
        self.assertTrue(isinstance(stmt.expression, Identifier))
        self.assertEqual("foobar", stmt.expression)
        self.assertEqual("foobar", stmt.token_literal())

    def test_integer_literal_expression(self):
        input = "5;"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))
        self.assertTrue(isinstance(stmt.expression, IntegerLiteral))
        self.assertEqual(5, stmt.expression)
        self.assertEqual("5", stmt.token_literal())

    def test_parsing_prefix_expressions(self):
        tests = [
            ("!5;", "!", 5),
            ("-15;", "-", 15),
            ("!foobar;", "!", "foobar"),
            ("-foobar;", "-", "foobar"),
            ("!true;", "!", True),
            ("!false;", "!", False)
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            self.assertEqual(len(program), 1)

            stmt = program[0]
            self.assertTrue(isinstance(stmt, ExpressionStatement))
            self.assertTrue(isinstance(stmt.expression, PrefixExpression))
            self.assertEqual(test[1], stmt.expression.operator)
            self.literal_expression_test_helper(stmt.expression.right, test[2])

    def test_parsing_infix_expressions(self):
        tests = [
            ("5 + 5;", 5, "+", 5),
            ("5 - 5;", 5, "-", 5),
            ("5 * 5;", 5, "*", 5),
            ("5 / 5;", 5, "/", 5),
            ("5 > 5;", 5, ">", 5),
            ("5 < 5;", 5, "<", 5),
            ("5 == 5;", 5, "==", 5),
            ("5 != 5;", 5, "!=", 5),
            ("5 >= 5;", 5, ">=", 5),
            ("5 <= 5;", 5, "<=", 5),
            ("foobar + barfoo;", "foobar", "+", "barfoo"),
            ("foobar - barfoo;", "foobar", "-", "barfoo"),
            ("foobar * barfoo;", "foobar", "*", "barfoo"),
            ("foobar / barfoo;", "foobar", "/", "barfoo"),
            ("foobar > barfoo;", "foobar", ">", "barfoo"),
            ("foobar < barfoo;", "foobar", "<", "barfoo"),
            ("foobar == barfoo;", "foobar", "==", "barfoo"),
            ("foobar != barfoo;", "foobar", "!=", "barfoo"),
            ("true == true", True, "==", True),
            ("true != false", True, "!=", False),
            ("false == false", False, "==", False)
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            self.assertEqual(len(program), 1)

            stmt = program[0]
            self.assertTrue(isinstance(stmt, ExpressionStatement))

            self.infix_expression_test_helper(stmt.expression, test[1], test[2], test[3])

    def test_parsing_infix_index_expressions(self):
        input = "a[i] + b[j];"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, InfixExpression))
        self.assertTrue(isinstance(exp.left, IndexExpression))
        self.assertTrue(isinstance(exp.right, IndexExpression))

    def test_parsing_infix_call_expressions(self):
        input = "call a(i) + call b(j);"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, InfixExpression))
        self.assertTrue(isinstance(exp.left, CallExpression))
        self.assertTrue(isinstance(exp.right, CallExpression))

    def test_operator_precedence_parsing(self):
        tests = [
            ("-a * b", "((-a) * b)"),
            ("!-a", "(!(-a))"),
            ("a + b + c", "((a + b) + c)"),
            ("a + b - c", "((a + b) - c)"),
            ("a * b * c", "((a * b) * c)"),
            ("a * b / c", "((a * b) / c)"),
            ("a + b / c", "(a + (b / c))"),
            ("a + b * c + d / e - f", "(((a + (b * c)) + (d / e)) - f)"),
            ("3 + 4; -5 * 5", "(3 + 4)((-5) * 5)"),
            ("5 > 4 == 3 < 4", "((5 > 4) == (3 < 4))"),
            ("5 < 4 != 3 > 4", "((5 < 4) != (3 > 4))"),
            ("3 + 4 * 5 == 3 * 1 + 4 * 5", "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)))"),
            ("true", "true"),
            ("false", "false"),
            ("3 > 5 == false", "((3 > 5) == false)"),
            ("3 < 5 == true", "((3 < 5) == true)"),
            ("1 + (2 + 3) + 4", "((1 + (2 + 3)) + 4)"),
            ("(5 + 5) * 2", "((5 + 5) * 2)"),
            ("2 / (5 + 5)", "(2 / (5 + 5))"),
            ("(5 + 5) * 2 * (5 + 5)", "(((5 + 5) * 2) * (5 + 5))"),
            ("-(5 + 5)", "(-(5 + 5))"),
            ("!(true == true)", "(!(true == true))"),
            ("a + call add(b * c) + d", "((a + call add((b * c))) + d)"),
            ("call add(a, b, 1, 2 * 3, 4 + 5, call add(6, 7 * 8))",
             "call add(a, b, 1, (2 * 3), (4 + 5), call add(6, (7 * 8)))"),
            ("call add(a + b + c * d / f + g)", "call add((((a + b) + ((c * d) / f)) + g))"),
            ("a * [1, 2, 3, 4][b * c] * d", "((a * ([1, 2, 3, 4][(b * c)])) * d)"),
            ("call add(a * b[2], b[1], 2 * [1, 2][1])", "call add((a * (b[2])), (b[1]), (2 * ([1, 2][1])))")
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)

            actual = str(program)
            self.assertEqual(test[1], actual)

    def test_boolean_expression(self):
        tests = [
            ("true", True),
            ("false", False)
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            self.assertEqual(len(program), 1)

            stmt = program[0]
            self.assertTrue(isinstance(stmt, ExpressionStatement))
            self.assertTrue(isinstance(stmt.expression, Boolean))
            self.assertEqual(test[1], stmt.expression)

    def test_if_expressions(self):
        input = "if x < y then x fi"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, IfExpression))

        self.infix_expression_test_helper(exp.condition, "x", "<", "y")

        self.assertEqual(len(exp.consequence.statements), 1)

        consequence = exp.consequence.statements[0]
        self.assertTrue(isinstance(consequence, ExpressionStatement))

        self.identifier_test_helper(consequence.expression, "x")

        self.assertTrue(isinstance(exp.alternative, Expression))

    def test_if_else_expressions(self):
        input = "if x < y then x else y fi"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, IfExpression))

        self.infix_expression_test_helper(exp.condition, "x", "<", "y")

        self.assertEqual(len(exp.consequence.statements), 1)

        consequence = exp.consequence.statements[0]
        self.assertTrue(isinstance(consequence, ExpressionStatement))

        self.identifier_test_helper(consequence.expression, "x")

        self.assertEqual(len(exp.alternative.statements), 1)

        alternative = exp.alternative.statements[0]
        self.assertTrue(isinstance(alternative, ExpressionStatement))

        self.identifier_test_helper(alternative.expression, "y")

    def test_while_expressions(self):
        input = "while x < y do x; od;"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        exp = stmt.expression
        self.assertTrue(isinstance(exp, WhileExpression))

        self.infix_expression_test_helper(exp.condition, "x", "<", "y")

        self.assertEqual(len(exp.body.statements), 1)

        body = exp.body.statements[0]
        self.assertTrue(isinstance(body, ExpressionStatement))

        self.identifier_test_helper(body.expression, "x")

    def test_function_literal_parsing(self):
        input = "function add(x, y) { x + y; }"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        self.assertTrue(isinstance(stmt, ExpressionStatement))

        function = stmt.expression
        self.assertTrue(isinstance(function, FunctionLiteral))
        self.assertEqual(2, len(function.parameters))

        self.literal_expression_test_helper(function.parameters[0], 'x')
        self.literal_expression_test_helper(function.parameters[1], 'y')

        self.assertEqual(1, len(function.body.statements))

        body_stmt = function.body.statements[0]
        self.assertTrue(isinstance(body_stmt, ExpressionStatement))

        self.infix_expression_test_helper(body_stmt.expression, 'x', '+', 'y')

    def test_function_parameter_parsing(self):
        tests = [
            ("function nothing() {return 5;};", []),
            ("function oneparam(x) {};", ['x']),
            ("function threeparams(x, y, z) {};", ['x', 'y', 'z'])
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            stmt = program[0]
            function = stmt.expression

            self.assertEqual(len(test[1]), len(function.parameters))

            for i, ident in enumerate(test[1]):
                self.literal_expression_test_helper(function.parameters[i], ident)

    def test_function_declarations_parsing(self):
        tests = [
            (
                "function declarations(i, j) var x, y; array[10] arr1, arr2; {let g <- 5;};",
                ['i', 'j'], [['x', 'y'], ['arr1', 'arr2']], 1
            )
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            stmt = program[0]
            function = stmt.expression

            self.assertTrue(isinstance(function, FunctionLiteral))

            parameters = function.parameters
            self.assertEqual(len(test[1]), len(parameters))
            for i, ident in enumerate(test[1]):
                self.literal_expression_test_helper(parameters[i], ident)

            declarations = function.declarations
            self.assertEqual(len(test[2]), len(declarations))
            for i, decl in enumerate(test[2]):
                self.assertEqual(len(decl), len(declarations[i]))
                for j, ident in enumerate(decl):
                    self.literal_expression_test_helper(declarations[i][j], ident)

            body = function.body
            self.assertEqual(test[3], len(body.statements))

    def test_procedure_parsing(self):
        tests = [
            ("procedure nothing {return 5;};", []),
            ("procedure declaration; var x; var i, j; {};", [1, 2])
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            stmt = program[0]
            function = stmt.expression

            self.assertEqual(len(test[1]), len(function.declarations))

            for i, ident in enumerate(test[1]):
                self.assertEqual(ident, len(function.declarations[i]))

    def test_main_parsing(self):
        input = "main" \
                " var i, j;" \
                " array[100] a" \
                " var x, y;" \
                " function add(a, b, c, d);" \
                " {" \
                "   let a <- b - c * d;" \
                "   return a / 2" \
                " };" \
                " procedure pass; " \
                " var b" \
                " {" \
                "   let b <- b * b;" \
                " };" \
                " {" \
                "  let a <- 200;" \
                "  let b <- 150;" \
                "  let c <- 50;" \
                "  let d <- 10;" \
                "  call add(a, b, c, d);" \
                " }."

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        main = stmt.expression
        self.assertEqual(MainLiteral, type(main))
        self.assertEqual(5, len(main.body))
        self.assertEqual(3, len(main.declarations))
        self.assertEqual(2, len(main.functions))

    def test_call_expression_parsing(self):
        input = "call add(1, 2 * 3, 4 + 5);"

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        self.assertEqual(len(program), 1)

        stmt = program[0]
        call = stmt.expression

        self.assertTrue(isinstance(call, CallExpression))

        self.identifier_test_helper(call.function, 'add')

        self.assertEqual(3, len(call.arguments))

        self.literal_expression_test_helper(call.arguments[0], 1)
        self.infix_expression_test_helper(call.arguments[1], 2, '*', 3)
        self.infix_expression_test_helper(call.arguments[2], 4, '+', 5)

    def test_call_expression_parameter_parsing(self):
        tests = [
            ("call add();", "add", []),
            ("call add(1);", "add", ['1']),
            ("call add(1, 2 * 3, 4 + 5);", "add", ['1', '(2 * 3)', '(4 + 5)'])
        ]

        for test in tests:
            l = Lexer(test[0])
            p = Parser(l)

            program = p.parse_program()

            self.check_parser_errors(p)
            stmt = program[0]
            call = stmt.expression

            self.assertTrue(isinstance(call, CallExpression))
            self.identifier_test_helper(call.function, test[1])
            self.assertEqual(len(test[2]), len(call.arguments))

            for i, arg in enumerate(test[2]):
                self.assertEqual(arg, str(call.arguments[i]))

    def test_string_literal_expression(self):
        input = '"hello world";'
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        literal = stmt.expression

        self.assertTrue(isinstance(literal, StringLiteral))
        self.assertEqual("hello world", literal.value)

    def test_parsing_empty_array_literals(self):
        input = "[]"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        array = stmt.expression

        self.assertTrue(isinstance(array, ArrayLiteral))
        self.assertEqual(0, len(array.elements))

    def test_parsing_array_literals(self):
        input = "[1, 2 * 2, 3 + 3]"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        array = stmt.expression

        self.assertTrue(isinstance(array, ArrayLiteral))
        self.assertEqual(3, len(array.elements))

        self.integer_literal_test_helper(array.elements[0], 1)
        self.infix_expression_test_helper(array.elements[1], 2, '*', 2)
        self.infix_expression_test_helper(array.elements[2], 3, '+', 3)

    def test_parsing_2d_array(self):
        input = "[[1, 2], [3, 4]]"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        array = stmt.expression

        self.assertTrue(isinstance(array, ArrayLiteral))
        self.assertEqual(2, len(array.elements))

        self.assertTrue(isinstance(array.elements[0], ArrayLiteral))
        self.assertTrue(isinstance(array.elements[1], ArrayLiteral))

    def test_parsing_index_expressions(self):
        input = "myArray[1 + 1]"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        ind_exp = stmt.expression

        self.assertTrue(isinstance(ind_exp, IndexExpression))
        self.identifier_test_helper(ind_exp.left, "myArray")
        self.infix_expression_test_helper(ind_exp.index, 1, '+', 1)

    def test_parsing_2d_index_expressions(self):
        input = "myArray[i][j]"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        ind_exp = stmt.expression

        self.assertTrue(isinstance(ind_exp, IndexExpression))
        self.assertTrue(isinstance(ind_exp.left, IndexExpression))
        self.identifier_test_helper(ind_exp.left.left, "myArray")
        self.literal_expression_test_helper(ind_exp.index, 'j')
        self.literal_expression_test_helper(ind_exp.left.index, 'i')

    def test_parsing_array_declaration(self):
        input = "array[5][6][7] myArray, anotherArray;"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        self.assertTrue(isinstance(stmt, ArrayDeclarationStatement))
        self.assertEqual(3, len(stmt.size))
        self.literal_expression_test_helper(stmt.size[0], 5)
        self.literal_expression_test_helper(stmt.size[1], 6)
        self.literal_expression_test_helper(stmt.size[2], 7)
        self.assertEqual(2, len(stmt.arrays))
        self.literal_expression_test_helper(stmt.arrays[0], 'myArray')
        self.literal_expression_test_helper(stmt.arrays[1], 'anotherArray')

    def test_parsing_call_expressions(self):
        input = "myArray[ call myFunc(i + j)]"
        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        ind_exp = stmt.expression

        self.assertTrue(isinstance(ind_exp, IndexExpression))
        self.identifier_test_helper(ind_exp.left, "myArray")
        self.assertTrue(isinstance(ind_exp.index, CallExpression))

    def test_parsing_empty_hash_literals(self):
        input = '{}'

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        hash = stmt.expression

        self.assertTrue(isinstance(hash, HashLiteral))
        self.assertEqual(0, len(hash.pairs))

    def test_parsing_hash_literals_string_keys(self):
        input = '{"one": 1, "two": 2, "three": 3}'
        expected = {"one": 1, "two": 2, "three": 3}

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        hash = stmt.expression

        self.assertTrue(isinstance(hash, HashLiteral))
        self.assertEqual(len(expected), len(hash.pairs))

        for key, value in hash.pairs.items():
            self.assertTrue(isinstance(key, StringLiteral))
            self.assertTrue(expected[str(key)], value)

    def test_parsing_hash_literals_boolean_keys(self):
        input = '{true: 1, false: 2}'
        expected = {"true": 1, "false": 2}

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        hash = stmt.expression

        self.assertTrue(isinstance(hash, HashLiteral))
        self.assertEqual(len(expected), len(hash.pairs))

        for key, value in hash.pairs.items():
            self.assertTrue(isinstance(key, Boolean))
            self.assertTrue(expected[str(key)], value)

    def test_parsing_hash_literals_integer_keys(self):
        input = '{1: 1, 2: 2, 3: 3}'
        expected = {"1": 1, "2": 2, "3": 3}

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        hash = stmt.expression

        self.assertTrue(isinstance(hash, HashLiteral))
        self.assertEqual(len(expected), len(hash.pairs))

        for key, value in hash.pairs.items():
            self.assertTrue(isinstance(key, IntegerLiteral))
            self.assertTrue(expected[str(key)], value)

    def test_parsing_hash_literals_with_expressions(self):
        input = '{"one": 0 + 1, "two": 10 - 8, "three": 15 / 5}'
        expected = {
            "one": [0, "+", 1],
            "two": [10, "-", 8],
            "three": [15, "/", 5]
        }

        l = Lexer(input)
        p = Parser(l)

        program = p.parse_program()

        self.check_parser_errors(p)
        stmt = program[0]
        hash = stmt.expression

        self.assertTrue(isinstance(hash, HashLiteral))
        self.assertEqual(3, len(hash.pairs))

        for key, value in hash.pairs.items():
            self.assertTrue(isinstance(key, StringLiteral))
            self.infix_expression_test_helper(value, *expected[str(key)])

    # Helper functions
    def let_statement_test_helper(self, s: LetStatement, name: str):
        self.assertEqual('let', s.token_literal())
        self.assertTrue(isinstance(s, LetStatement))
        self.assertEqual(name, s.name.token_literal())
        self.assertEqual(name, s.name.expression)

    def infix_expression_test_helper(self, exp: InfixExpression, left: Union[str, int], operator: str,
                                     right: Union[str, int]):
        self.assertTrue(isinstance(exp, InfixExpression))

        self.literal_expression_test_helper(exp.left, left)
        self.assertEqual(operator, exp.operator)
        self.literal_expression_test_helper(exp.right, right)

    def literal_expression_test_helper(self, exp: Union[IntegerLiteral, Identifier, Boolean],
                                       expected: Union[str, int, bool]):
        self.assertIn(type(expected), [str, int, bool])

        if isinstance(expected, str):
            self.identifier_test_helper(exp, expected)
        elif isinstance(expected, bool):
            self.boolean_literal_test_helper(exp, expected)
        elif isinstance(expected, int):
            self.integer_literal_test_helper(exp, expected)

    def integer_literal_test_helper(self, il: IntegerLiteral, value: int):
        self.assertTrue(isinstance(il, IntegerLiteral))
        self.assertEqual(value, il.value)
        self.assertEqual(f'{value}', il.token_literal())

    def identifier_test_helper(self, exp: Identifier, value: str):
        self.assertTrue(isinstance(exp, Identifier))
        self.assertEqual(value, exp.value)
        self.assertEqual(value, exp.token_literal())

    def boolean_literal_test_helper(self, bl: Boolean, value: bool):
        self.assertTrue(isinstance(bl, Boolean))
        self.assertEqual(value, bl.value)
        self.assertEqual(f'{value}'.lower(), bl.token_literal())
