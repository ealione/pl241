# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from functools import reduce
from typing import Optional, Tuple, List, Union

from _exceptions import CompilationError
from ast import *
from objects import *
from opcodes import *
from .block import BlockChain
from .bytecode import Bytecode
from .symbol_table import SymbolTable, Symbol


@dataclass
class EmittedInstruction:
    opcode: OpCode = field(default_factory=OpCode)
    position: int = field(default=0)


@dataclass
class CompilationScope:
    instructions: Instructions = field(default_factory=Instructions)
    last_instruction: EmittedInstruction = field(default_factory=EmittedInstruction)
    previous_instruction: EmittedInstruction = field(default_factory=EmittedInstruction)
    blockchain: BlockChain = field(default_factory=BlockChain)


@dataclass
class Compiler:
    constants: List[Object] = field(default_factory=list)
    symbol_table: SymbolTable = field(default_factory=SymbolTable)
    scopes: List[CompilationScope] = field(default_factory=list)
    scope_index: int = field(default=0)
    _symbol_register: List[Symbol] = field(default_factory=list)
    _value_register: List[int] = field(default_factory=list)
    _pc_register: List[int] = field(default_factory=list)
    _rt_register: List[int] = field(default_factory=list)

    def __post_init__(self):
        self.scopes.append(CompilationScope())

    @classmethod
    def with_state(cls, s: SymbolTable, constants: List[Object]) -> Compiler:
        return cls(symbol_table=s, constants=constants)

    def compile(self, node) -> Optional[bool, CompilationError]:
        node_type = type(node)
        if node_type == Program:
            for stmt in node.statements:
                err = self.compile(stmt)
                if err:
                    return err
        elif node_type == ExpressionStatement:
            err = self.compile(node.expression)
            if err:
                return err
        elif node_type == BlockStatement:
            for stmt in node.statements:
                err = self.compile(stmt)
                if err:
                    return err
        elif node_type == MainLiteral:
            # TODO: check for correctness
            err = self.compile(node.declarations)
            if err:
                return err
            err = self.compile(node.functions)
            if err:
                return err
            err = self.compile(node.body)
            if err:
                return err
        elif node_type == IndexExpression:
            err = self.compile(node.left)
            if err:
                return err

            left_pos = self.last_instruction_position

            if isinstance(node.left, Identifier):
                array_idx = self._symbol_register[-1]
                array_obj = self.constants[array_idx.index]
                self._value_register.extend(array_obj.dimensions[1:] + [1])

            step = reduce((lambda x, y: x * y), self._value_register)
            self._value_register.pop(0)

            err = self.compile(node.index)
            if err:
                return err
            if isinstance(node.index, IntegerLiteral):
                self.emit(OpMul, ConstantOperand(value=node.index.value), ConstantOperand(value=step))
            elif isinstance(node.index, Identifier):
                symbol = self._symbol_register.pop()
                self.emit(OpMul, LiteralOperand(value=symbol.name, index=symbol.address[-1]),
                          ConstantOperand(value=step))
            else:
                self.emit(OpMul, AddressOperand(value=self.last_instruction_position), ConstantOperand(value=step))

            self.emit(OpAdd, AddressOperand(value=left_pos), AddressOperand(value=self.last_instruction_position))
        elif node_type == InfixExpression:
            if isinstance(node.left, Identifier):
                symbol, ok = self.symbol_table.resolve(node.left.value)
                left = LiteralOperand(value=symbol.name, index=symbol.address[-1])
            elif isinstance(node.left, IntegerLiteral):
                left = ConstantOperand(value=node.left.value)
            else:
                err = self.compile(node.left)
                if err:
                    return err
                left = AddressOperand(value=self.last_instruction_position)

            if isinstance(node.right, Identifier):
                symbol, ok = self.symbol_table.resolve(node.right.value)
                right = LiteralOperand(value=symbol.name, index=symbol.address[-1])
            elif isinstance(node.right, IntegerLiteral):
                right = ConstantOperand(value=node.right.value)
            else:
                err = self.compile(node.right)
                if err:
                    return err
                right = AddressOperand(value=self.last_instruction_position)

            operator = node.operator
            if operator in ["<", ">", "<=", ">=", "==", "!="]:
                self.emit(OpCmp, left, right)

            if operator == "<":
                self.emit(OpGreaterEqual, AddressOperand(value=self.last_instruction_position),
                          AddressOperand(value=9999))
            elif operator == ">":
                self.emit(OpLessEqual, AddressOperand(value=self.last_instruction_position), AddressOperand(value=9999))
            elif operator == ">=":
                self.emit(OpLessThan, AddressOperand(value=self.last_instruction_position), AddressOperand(value=9999))
            elif operator == "<=":
                self.emit(OpGreaterThan, AddressOperand(value=self.last_instruction_position),
                          AddressOperand(value=9999))
            elif operator == "==":
                self.emit(OpNotEqual, AddressOperand(value=self.last_instruction_position), AddressOperand(value=9999))
            elif operator == "!=":
                self.emit(OpEqual, AddressOperand(value=self.last_instruction_position), AddressOperand(value=9999))
            elif operator == "+":
                self.emit(OpAdd, left, right)
            elif operator == "-":
                self.emit(OpSub, left, right)
            elif operator == "*":
                self.emit(OpMul, left, right)
            elif operator == "/":
                self.emit(OpDiv, left, right)
            else:
                return CompilationError(node, f"Operator not recognized '{operator}'")
        elif node_type == PrefixExpression:
            err = self.compile(node.right)
            if err:
                return err
            pos = len(self.current_instructions)
            if node.operator == "-":
                self.emit(OpMinus, AddressOperand(value=pos))
            else:
                return CompilationError(node, f"Unkown operator '{node.operator}'")
        elif node_type == Identifier:
            symbol, ok = self.symbol_table.resolve(node.value)
            if not ok:
                return CompilationError(node, f"Variable not recognized '{node.value}'")
            if symbol.name in self.current_blockchain.current_block.touched_symbols:
                symbol = self.current_blockchain.current_block.touched_symbols[symbol.name]
            self._symbol_register.append(symbol)
            if len(symbol.address) == 1:
                self.emit(OpLoad, LiteralOperand(value=symbol.name, index=0))
        elif node_type == IfExpression:
            err = self.compile(node.condition)
            if err:
                return err
            jump_pos = self.last_instruction_position

            self.add_block(instruction_index=jump_pos, type='consequence')

            err = self.compile(node.consequence)
            if err:
                return err

            if isinstance(node.alternative, BlockStatement):
                self.change_branch_target(jump_pos, AddressOperand(value=self.last_instruction_position + 2))
                jump_pos = self.emit(OpBranch, AddressOperand(value=9999))

                for n, s in self.current_blockchain.current_block.touched_symbols.items():
                    symbol, ok = self.symbol_table.resolve(n)
                    del symbol.address[-len(s.address):]

                self.add_block(instruction_index=jump_pos, type='alternative')

                err = self.compile(node.alternative)
                if err:
                    return err

            self.change_branch_target(jump_pos, AddressOperand(value=self.last_instruction_position + 1))
            self.add_block(instruction_index=self.last_instruction_position)
        elif node_type == LetStatement:
            err = self.compile(node.name.expression)
            if err:
                return err

            symbol = self._symbol_register.pop()
            symbol.address.append(self.last_instruction_position)
            new_symbol = Symbol(name=symbol.name, index=symbol.index)
            new_symbol.address.append(self.last_instruction_position)
            self.current_blockchain.current_block.update_touched_symbols(new_symbol)

            if isinstance(node.name.expression, Identifier):
                right_operand = LiteralOperand(value=symbol.name, index=symbol.address[-1])
            else:
                right_operand = AddressOperand(value=self.last_instruction_position)

            if isinstance(node.value, IntegerLiteral):
                left_operand = ConstantOperand(value=node.value.value)
            elif isinstance(node.value, Identifier):
                err = self.compile(node.value)
                if err:
                    return err
                left_symbol = self._symbol_register.pop()
                left_operand = LiteralOperand(value=left_symbol.name, index=left_symbol.address[-1])
            else:
                err = self.compile(node.value)
                if err:
                    return err
                left_operand = AddressOperand(value=self.last_instruction_position)
            self.emit(OpMove, left_operand, right_operand)
        elif node_type == WhileExpression:
            s_pos = len(self.current_instructions)
            err = self.compile(node.condition)
            if err:
                return err
            jump_not_truthy_pos = self.last_instruction_position

            self.add_block(instruction_index=jump_not_truthy_pos, type='consequence-while')

            err = self.compile(node.body)
            if err:
                return err

            self.emit(OpBranch, AddressOperand(value=s_pos))

            after_body_pos = len(self.current_instructions)
            self.change_branch_target(jump_not_truthy_pos, AddressOperand(value=after_body_pos))

            self.add_block(instruction_index=self.last_instruction_position)
        elif node_type == FunctionLiteral:
            self.symbol_table.define(name=node.token.literal)
            symbol, ok = self.symbol_table.resolve(node.token.literal)
            self.enter_scope()

            for parameter in node.parameters:
                self.symbol_table.define(parameter.value)

            err = self.compile(node.declarations)
            if err:
                return err

            err = self.compile(node.body)
            if err:
                return err

            self.emit(OpBranch, LiteralOperand(value='PC', index=0))

            free_symbols = self.symbol_table.free_symbols
            num_locals = self.symbol_table.num_definitions
            instructions, blockchain = self.leave_scope()

            compiled_function = CompiledFunctionObject(
                parameters_count=len(node.parameters),
                locals_count=num_locals,
                parameters=node.parameters,
                bytecode=Bytecode(instructions, self.constants, blockchain),
                symbol=symbol)
            fn_index = self.add_constant(compiled_function)
            symbol.index = fn_index
        elif node_type == ProcedureLiteral:
            self.symbol_table.define(name=node.token.literal)
            symbol, ok = self.symbol_table.resolve(node.token.literal)
            self.enter_scope()

            for parameter in node.parameters:
                self.symbol_table.define(parameter.value)

            err = self.compile(node.declarations)
            if err:
                return err

            err = self.compile(node.body)
            if err:
                return err

            self.emit(OpBranch, LiteralOperand(value='PC', index=0))

            free_symbols = self.symbol_table.free_symbols
            num_locals = self.symbol_table.num_definitions
            instructions, blockchain = self.leave_scope()

            compiled_procedure = CompiledProcedureObject(
                parameters_count=len(node.parameters),
                locals_count=num_locals,
                parameters=node.parameters,
                bytecode=Bytecode(instructions, self.constants, blockchain),
                symbol=symbol)
            fn_index = self.add_constant(compiled_procedure)
            symbol.index = fn_index
        elif node_type == CallExpression:
            fname = node.function
            if fname == 'InputNum':
                self.emit(OpRead)
            elif fname == 'OutputNewLine':
                self.emit(OpWriteNL)
            elif fname == 'OutputNum':
                if isinstance(node.arguments[0], IntegerLiteral):
                    self.emit(OpWrite, ConstantOperand(value=node.arguments[0].value))
                elif isinstance(node.arguments[0], Identifier):
                    symbol, ok = self.symbol_table.resolve(name=node.arguments[0].value)
                    self.emit(OpWrite, LiteralOperand(value=symbol.name, index=symbol.address[-1]))
                else:
                    return False
            else:
                symbol, ok = self.symbol_table.resolve(fname.value)
                func = self.constants[symbol.index]
                for par, arg in zip(func.parameters, node.arguments):
                    if type(arg) == IntegerLiteral:
                        self.emit(OpStore, ConstantOperand(value=arg), LiteralOperand(value=par, index=0))
                    elif type(arg) == Identifier:
                        self.emit(OpStore, LiteralOperand(value=arg, index=0), LiteralOperand(value=par, index=0))
                    else:
                        self.compile(arg)
                        self.emit(OpStore, AddressOperand(value=self.last_instruction_position),
                                  LiteralOperand(value=par, index=0))
                self.emit(OpStore, AddressOperand(value=self.last_instruction_position + 2),
                          LiteralOperand(value='PC', index=0))
                self.emit(OpBranch, LiteralOperand(value=fname.value, index=symbol.index))
                if not isinstance(func, CompiledProcedureObject):
                    self.emit(OpLoad, LiteralOperand(value='RT', index=0))
        elif node_type == VariableDeclarationStatement:
            # TODO: don't load declarations
            for symbol in node.variables:
                self.symbol_table.define(name=symbol.value)
        elif node_type == ArrayDeclarationStatement:
            # TODO: don't load declarations
            for symbol in node.arrays:
                self.symbol_table.define(name=symbol.value)
                symbol, ok = self.symbol_table.resolve(symbol.value)
                ar_index = self.add_constant(
                    ArrayDeclarationObject(symbol=symbol, dimensions=[i.value for i in node.size]))
                symbol.index = ar_index
        elif node_type == CommentStatement:
            return False
        elif node_type == IntegerLiteral:
            pass
        elif node_type == ReturnStatement:
            if type(node.value) == IntegerLiteral:
                self.emit(OpStore, ConstantOperand(value=node.value), LiteralOperand(value='RT', index=0))
            elif type(node.value) == Identifier:
                symbol, ok = self.symbol_table.resolve(node.value.value)
                self.emit(OpStore, LiteralOperand(value=node.value, index=symbol.address[-1]),
                          LiteralOperand(value='RT', index=0))
            else:
                self.compile(node.value)
                self.emit(OpStore, AddressOperand(value=self.last_instruction_position),
                          LiteralOperand(value='RT', index=0))
        else:
            return CompilationError(node, f"Unrecognized statement '{node_type}'")

    @property
    def byte_code(self) -> Bytecode:
        return Bytecode(self.current_instructions, self.constants, self.current_blockchain)

    def add_constant(self, o: Object) -> int:
        self.constants.append(o)
        return len(self.constants) - 1

    def emit(self, op: OpCode, *operands: Operand) -> int:
        ins = IRInstruction(op, *operands)
        pos = self.add_instruction(ins)

        self.set_last_instruction(op, pos)

        return pos

    def add_instruction(self, ins: Union[IRInstruction, Instruction]) -> int:
        pos_new_instruction = len(self.current_instructions)
        self.current_instructions.append(ins)

        self.scopes[self.scope_index].instructions = self.current_instructions

        return pos_new_instruction

    def add_block(self, instruction_index: int, type: str = 'flow') -> None:
        self.scopes[self.scope_index].blockchain.new_block(instruction_index, type)

    def set_last_instruction(self, op: OpCode, pos: int) -> None:
        previous = self.scopes[self.scope_index].last_instruction
        last = EmittedInstruction(opcode=op, position=pos)

        self.scopes[self.scope_index].previous_instruction = previous
        self.scopes[self.scope_index].last_instruction = last

    def last_instruction_is(self, op: Union[OpCode, List[OpCode]]) -> bool:
        if not isinstance(op, list):
            op = [op]
        if len(self.current_instructions):
            return self.scopes[self.scope_index].last_instruction.opcode in op

    def replace_instruction(self, pos: int, new_instruction: Union[IRInstruction, Instruction]) -> None:
        ins = self.current_instructions
        ins[pos] = new_instruction

    def change_operands(self, op_pos: int, operand: Operand) -> None:
        op = self.current_instructions[op_pos].type
        new_instruction = IRInstruction(op, operand)

        self.replace_instruction(op_pos, new_instruction)

    def change_branch_target(self, op_pos: int, operand: Operand) -> None:
        new_instruction = self.current_instructions[op_pos]
        if self.check_if_is_branch(new_instruction.type):
            new_instruction.operands[-1] = operand

            self.replace_instruction(op_pos, new_instruction)

    @staticmethod
    def check_if_is_branch(op: Operand):
        return op in [OpNotEqual, OpEqual, OpLessEqual, OpLessThan, OpGreaterEqual, OpGreaterThan, OpBranch]

    @property
    def current_instructions(self) -> Instructions:
        return self.scopes[self.scope_index].instructions

    @property
    def last_instruction_position(self) -> int:
        return len(self.current_instructions) - 1

    @property
    def current_blockchain(self) -> BlockChain:
        return self.scopes[self.scope_index].blockchain

    def enter_scope(self) -> None:
        self.scopes.append(CompilationScope())
        self.scope_index += 1

        self.symbol_table = SymbolTable.enclosed_symbol_table(self.symbol_table)

    def leave_scope(self) -> Tuple[Instructions, BlockChain]:
        instructions = self.current_instructions
        blockchain = self.current_blockchain
        self.scopes.pop()
        self.scope_index -= 1

        self.symbol_table = self.symbol_table.outer

        return instructions, blockchain
