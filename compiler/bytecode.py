# -*- encoding: utf-8 -*-
from dataclasses import dataclass, field
from textwrap import indent
from typing import Tuple, List

from objects import Object
from opcodes import *
from .block import BlockChain


@dataclass
class Bytecode:
    instructions: Instructions = field(default_factory=Instructions)
    constants: List[Object] = field(default_factory=list)
    blocks: BlockChain = field(default_factory=list)

    def instructions_in_block(self, block_idx: int) -> Instructions:
        if self.blocks[block_idx].stop <= 0:
            self.blocks[block_idx].stop = len(self.instructions)
        return Instructions(self.instructions[self.blocks[block_idx].start: self.blocks[block_idx].stop + 1],
                            offset_start=self.blocks[block_idx].start)

    def __str__(self):
        return "\n".join(
            f"B{i}\n{indent(str(self.instructions_in_block(i)), prefix='  ')}\n" for i, b in enumerate(self.blocks))

    def __key(self) -> Tuple[Instructions, List[Object]]:
        return self.instructions, self.constants

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"
