# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from typing import List, Tuple, Dict

from .symbol_table import Symbol


@dataclass
class Block:
    start: int = field(default=0)
    stop: int = field(default=0)
    touched_symbols: Dict[str, Symbol] = field(default_factory=dict)
    parent: List[int] = field(default_factory=list)
    children: List[int] = field(default_factory=list)

    def update_touched_symbols(self, symbol: Symbol) -> None:
        if symbol in self.touched_symbols:
            self.touched_symbols[symbol.name].address.extend(symbol.address)
        else:
            self.touched_symbols[symbol.name] = symbol

    def __str__(self) -> str:
        symbols = ", ".join(f"{str(s)}" for s in self.touched_symbols)
        return f"Symbols [{symbols}] Parents {self.parent} Children {self.children}"

    def __key(self) -> Tuple[int, int]:
        return self.start, self.stop

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class BlockChain:
    blocks: List[Block] = field(default_factory=list)
    _parents: List[int] = field(default_factory=list)
    _last_index: int = field(default=0)

    def __post_init__(self) -> None:
        self.blocks.append(Block())

    @property
    def current_block(self) -> Block:
        return self.blocks[-1]

    def __len__(self) -> int:
        return len(self.blocks)

    def __str__(self) -> str:
        return "\n".join(f"B{i}: {str(b)}" for i, b in enumerate(self.blocks))

    def __getitem__(self, i: int) -> Block:
        return self.blocks[i]

    def new_block(self, instruction_index: int, type: str = 'flow'):
        cur_index = self.__len__()
        self.current_block.stop = instruction_index
        block = Block(start=instruction_index + 1)
        if type == 'consequence':
            self._last_index = cur_index
            self._parents.append(cur_index - 1)
            block.parent.append(cur_index - 1)
            self.blocks[cur_index - 1].children.append(cur_index)
        elif type == 'consequence-while':
            self._last_index = cur_index
            self._parents.append(cur_index - 1)
            block.parent.append(cur_index - 1)
            block.children.append(cur_index - 1)
            self.blocks[cur_index - 1].children.append(cur_index)
            self.blocks[cur_index - 1].parent.append(cur_index)
        elif type == 'alternative':
            parent_block = self._parents.pop()
            self._parents.append(cur_index)
            block.parent.append(parent_block)
            self.blocks[parent_block].children.append(cur_index)
        elif type == 'flow':
            left_parent = self._parents.pop()
            right_parent = self._last_index
            if left_parent == right_parent:
                block.parent.append(left_parent)
                self.blocks[left_parent].children.append(cur_index)
            else:
                block.parent.extend([left_parent, right_parent])
                self.blocks[left_parent].children.append(cur_index)
                self.blocks[right_parent].children.append(cur_index)
            self._last_index = cur_index
        self.blocks.append(block)

    def __key(self) -> str:
        return str(self.blocks)

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"
