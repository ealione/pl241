# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from textwrap import shorten
from typing import Final, Dict, List, NewType, Tuple, Union

SymbolScope = NewType('SymbolScope', str)

LocalScope: Final[SymbolScope] = SymbolScope("LOCAL")
GlobalScope: Final[SymbolScope] = SymbolScope("GLOBAL")
BuiltinScope: Final[SymbolScope] = SymbolScope("BUILTIN")
FreeScope: Final[SymbolScope] = SymbolScope("FREE")


@dataclass
class Symbol:
    name: str
    index: int
    scope: SymbolScope = field(default_factory=str)
    address: List[int] = field(default_factory=list)

    def __post_init__(self) -> None:
        self.address.append(0)

    def __str__(self) -> str:
        return f'{str(self.scope)}@{self.name}'

    def __key(self) -> Tuple[str, int, SymbolScope]:
        return self.name, self.index, self.scope

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"


@dataclass
class SymbolTable:
    outer: SymbolTable = field(default=None)
    store: Dict[str, Symbol] = field(default_factory=dict)
    num_definitions: int = field(default=0)
    free_symbols: List[Symbol] = field(default_factory=list)

    @classmethod
    def enclosed_symbol_table(cls, outer: SymbolTable):
        return cls(outer=outer)

    def __chart__(self) -> str:
        s = f"Register Table [{self.num_definitions}: definitions]\n"
        for key in self.store.keys():
            s += f'{str(self.store[key])}\n'
        return s

    def define(self, name: str) -> Symbol:
        symbol = Symbol(name=name, index=0)
        if not self.outer:
            symbol.scope = GlobalScope
        else:
            symbol.scope = LocalScope
        self.store[name] = symbol
        self.num_definitions += 1
        return symbol

    def resolve(self, name: str) -> Tuple[Symbol, bool]:
        obj, ok = self.store.get(name, None), name in self.store
        if not ok and self.outer is not None:
            obj, ok = self.outer.resolve(name)
            if not ok:
                return obj, ok
            if obj.scope == GlobalScope or obj.scope == BuiltinScope:
                return obj, ok
            free = self.define_free(obj)
            return free, True
        return obj, ok

    def define_builtin(self, index: int, name: str):
        symbol = Symbol(name=name, index=index, scope=BuiltinScope)
        self.store[name] = symbol
        return symbol

    def define_free(self, original: Symbol):
        self.free_symbols.append(original)
        symbol = Symbol(name=original.name, index=len(self.free_symbols) - 1, scope=FreeScope)
        self.store[original.name] = symbol
        return symbol

    def __str__(self) -> str:
        return f"[{'GLOBAL' if self.outer is None else 'LOCAL'}] Symbol Table [{self.num_definitions}: definitions]" \
               f", Symbols {shorten(str(list(self.store.keys())), width=70)}" \
               f", Free symbols {shorten(str(self.free_symbols), width=70)}"

    def __len__(self) -> int:
        return self.num_definitions

    def __getitem__(self, i: Union[int, str]) -> Tuple[Symbol, bool]:
        if isinstance(i, int):
            return self.free_symbols[i], i < len(self.free_symbols)
        elif isinstance(i, str):
            return self.resolve(i)

    def __delitem__(self, i: Union[int, str]) -> None:
        if isinstance(i, int):
            del self.free_symbols[i]
        elif isinstance(i, str):
            del self.store[i]

    def __key(self) -> Tuple[SymbolTable, Dict[str, Symbol], int, List[Symbol]]:
        return self.outer, self.store, self.num_definitions, self.free_symbols

    def __hash__(self) -> int:
        return hash(self.__key())

    def __repr__(self) -> str:
        return f"{self.__class__.__name__} {hex(id(self))} [{self.__hash__()}]"

    def clear(self):
        return self.store.clear()

    def has_key(self, k):
        return k in self.store

    def update(self, *args, **kwargs):
        return self.store.update(*args, **kwargs)

    def keys(self):
        return self.store.keys()

    def values(self):
        return self.store.values()

    def items(self):
        return self.store.items()

    def __contains__(self, item):
        return item in self.__dict__

    def __iter__(self):
        return iter(self.store)
