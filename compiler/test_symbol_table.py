# -*- encoding: utf-8 -*-
from unittest import TestCase

from .symbol_table import SymbolTable, Symbol, GlobalScope, LocalScope, BuiltinScope, FreeScope


class TestLexer(TestCase):

    def test_define(self):
        expected = {
            "a": Symbol(name="a", scope=GlobalScope, index=0),
            "b": Symbol(name="b", scope=GlobalScope, index=1),
            "c": Symbol(name="c", scope=LocalScope, index=0),
            "d": Symbol(name="d", scope=LocalScope, index=1),
            "e": Symbol(name="e", scope=LocalScope, index=0),
            "f": Symbol(name="f", scope=LocalScope, index=1)
        }

        global_scope = SymbolTable()

        a = global_scope.define("a")
        self.assertEqual(expected["a"], a)

        b = global_scope.define("b")
        self.assertEqual(expected["b"], b)

        first_local = SymbolTable.enclosed_symbol_table(global_scope)

        c = first_local.define("c")
        self.assertEqual(expected["c"], c)

        d = first_local.define("d")
        self.assertEqual(expected["d"], d)

        second_local = SymbolTable.enclosed_symbol_table(first_local)

        e = second_local.define("e")
        self.assertEqual(expected["e"], e)

        f = second_local.define("f")
        self.assertEqual(expected["f"], f)

    def test_resolve_global(self):
        global_scope = SymbolTable()
        global_scope.define("a")
        global_scope.define("b")

        expected = [
            Symbol(name="a", scope=GlobalScope, index=0),
            Symbol(name="b", scope=GlobalScope, index=1)
        ]

        for symbol in expected:
            result, ok = global_scope[symbol.name]
            self.assertTrue(ok)
            self.assertEqual(symbol, result)

    def test_resolve_local(self):
        global_scope = SymbolTable()
        global_scope.define("a")
        global_scope.define("b")

        local_scope = SymbolTable.enclosed_symbol_table(global_scope)
        local_scope.define("c")
        local_scope.define("d")

        expected = [
            Symbol(name="a", scope=GlobalScope, index=0),
            Symbol(name="b", scope=GlobalScope, index=1),
            Symbol(name="c", scope=LocalScope, index=0),
            Symbol(name="d", scope=LocalScope, index=1)
        ]

        for symbol in expected:
            result, ok = local_scope[symbol.name]
            self.assertTrue(ok)
            self.assertEqual(symbol, result)

    def test_resolve_nested_local(self):
        global_scope = SymbolTable()
        global_scope.define("a")
        global_scope.define("b")

        first_local = SymbolTable.enclosed_symbol_table(global_scope)
        first_local.define("c")
        first_local.define("d")

        second_local = SymbolTable.enclosed_symbol_table(first_local)
        second_local.define("e")
        second_local.define("f")

        tests = [
            (
                first_local,
                [
                    Symbol(name="a", scope=GlobalScope, index=0),
                    Symbol(name="b", scope=GlobalScope, index=1),
                    Symbol(name="c", scope=LocalScope, index=0),
                    Symbol(name="d", scope=LocalScope, index=1)
                ]
            ),
            (
                second_local,
                [
                    Symbol(name="a", scope=GlobalScope, index=0),
                    Symbol(name="b", scope=GlobalScope, index=1),
                    Symbol(name="e", scope=LocalScope, index=0),
                    Symbol(name="f", scope=LocalScope, index=1)
                ]
            )
        ]

        for test in tests:
            for symbol in test[1]:
                result, ok = test[0][symbol.name]
                self.assertTrue(ok)
                self.assertEqual(symbol, result)

    def test_define_resolve_builtins(self):
        expected = [
            Symbol(name="a", scope=BuiltinScope, index=0),
            Symbol(name="c", scope=BuiltinScope, index=1),
            Symbol(name="e", scope=BuiltinScope, index=2),
            Symbol(name="f", scope=BuiltinScope, index=3)
        ]

        global_scope = SymbolTable()
        first_local = SymbolTable.enclosed_symbol_table(global_scope)
        second_local = SymbolTable.enclosed_symbol_table(first_local)

        for i, symbol in enumerate(expected):
            global_scope.define_builtin(i, symbol.name)

        for table in [global_scope, first_local, second_local]:
            for symbol in expected:
                result, ok = table.resolve(symbol.name)
                self.assertTrue(ok)
                self.assertEqual(symbol, result)

    def test_resolve_free(self):
        global_scope = SymbolTable()
        global_scope.define("a")
        global_scope.define("b")

        first_local = SymbolTable.enclosed_symbol_table(global_scope)
        first_local.define("c")
        first_local.define("d")

        second_local = SymbolTable.enclosed_symbol_table(first_local)
        second_local.define("e")
        second_local.define("f")

        tests = [
            (
                first_local,
                [
                    Symbol(name="a", scope=GlobalScope, index=0),
                    Symbol(name="b", scope=GlobalScope, index=1),
                    Symbol(name="c", scope=LocalScope, index=0),
                    Symbol(name="d", scope=LocalScope, index=1)
                ],
                [

                ]
            ),
            (
                second_local,
                [
                    Symbol(name="a", scope=GlobalScope, index=0),
                    Symbol(name="b", scope=GlobalScope, index=1),
                    Symbol(name="c", scope=FreeScope, index=0),
                    Symbol(name="d", scope=FreeScope, index=1),
                    Symbol(name="e", scope=LocalScope, index=0),
                    Symbol(name="f", scope=LocalScope, index=1)
                ],
                [
                    Symbol(name="c", scope=LocalScope, index=0),
                    Symbol(name="d", scope=LocalScope, index=1)
                ]
            )
        ]

        for test in tests:
            for symbol in test[1]:
                result, ok = test[0].resolve(symbol.name)
                self.assertTrue(ok)
                self.assertEqual(symbol, result)

            self.assertEqual(len(test[2]), len(test[0].free_symbols))

            for i, symbol in enumerate(test[2]):
                result, ok = test[0][i]
                self.assertEqual(symbol, result)

    def test_resolve_unresolvable_free(self):
        global_scope = SymbolTable()
        global_scope.define("a")

        first_local = SymbolTable.enclosed_symbol_table(global_scope)
        first_local.define("c")

        second_local = SymbolTable.enclosed_symbol_table(first_local)
        second_local.define("e")
        second_local.define("f")

        expected = [
            Symbol(name="a", scope=GlobalScope, index=0),
            Symbol(name="c", scope=FreeScope, index=0),
            Symbol(name="e", scope=LocalScope, index=0),
            Symbol(name="f", scope=LocalScope, index=1)
        ]

        for symbol in expected:
            result, ok = second_local.resolve(symbol.name)
            self.assertTrue(ok)
            self.assertEqual(symbol, result)

        expected_unresolvable = [
            "b",
            "d"
        ]

        for name in expected_unresolvable:
            _, ok = second_local.resolve(name)
            self.assertFalse(ok)
