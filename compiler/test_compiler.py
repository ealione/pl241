# -*- encoding: utf-8 -*-
from typing import Tuple, List, Union
from unittest import TestCase

from ast import *
from graph import control_flow_graph
from lexer import *
from objects import *
from opcodes import *
from parser import *
from .compiler import Compiler


class TestCode(TestCase):
    def test_compiler_scopes(self):
        compiler = Compiler()

        self.assertEqual(0, compiler.scope_index)

        global_symbol_table = compiler.symbol_table

        compiler.emit(OpMul)

        compiler.enter_scope()
        self.assertEqual(1, compiler.scope_index)

        compiler.emit(OpSub)
        self.assertEqual(1, len(compiler.current_instructions))

        self.assertEqual(OpSub, compiler.scopes[compiler.scope_index].last_instruction.opcode)

        self.assertEqual(global_symbol_table, compiler.symbol_table.outer)

        compiler.leave_scope()
        self.assertEqual(0, compiler.scope_index)
        self.assertEqual(global_symbol_table, compiler.symbol_table)
        self.assertEqual(None, compiler.symbol_table.outer)

        compiler.emit(OpAdd)
        self.assertEqual(2, len(compiler.scopes[compiler.scope_index].instructions))
        self.assertEqual(OpAdd, compiler.scopes[compiler.scope_index].last_instruction.opcode)
        self.assertEqual(OpMul, compiler.scopes[compiler.scope_index].previous_instruction.opcode)

    def test_conditional(self):
        tests = [
            # (
            #     "var a, b; let a <- 5; let a <- b;",
            #     [],
            #     [
            #         Instructions([
            #             IRInstruction(OpConstant, ConstantOperand(value=1)),
            #             IRInstruction(OpConstant, ConstantOperand(value=2)),
            #             IRInstruction(OpCmp, AddressOperand(value=0), AddressOperand(value=1)),
            #             IRInstruction(OpGreaterEqual, AddressOperand(value=2), AddressOperand(value=7)),
            #             IRInstruction(OpConstant, ConstantOperand(value=10)),
            #             IRInstruction(OpLoad, LiteralOperand(value='GLOBAL@a_0')),
            #             IRInstruction(OpMove, AddressOperand(value=5), LiteralOperand(value='GLOBAL@a_0'))
            #         ])
            #     ]
            # ),
            # (
            #     "var a; if (1 < 2) then let a <- 10 fi",
            #     [],
            #     [
            #         Instructions([
            #             IRInstruction(OpConstant, ConstantOperand(value=1)),
            #             IRInstruction(OpConstant, ConstantOperand(value=2)),
            #             IRInstruction(OpCmp, AddressOperand(value=0), AddressOperand(value=1)),
            #             IRInstruction(OpGreaterEqual, AddressOperand(value=2), AddressOperand(value=7)),
            #             IRInstruction(OpConstant, ConstantOperand(value=10)),
            #             IRInstruction(OpLoad, LiteralOperand(value='GLOBAL@a_0')),
            #             IRInstruction(OpMove, AddressOperand(value=5), LiteralOperand(value='GLOBAL@a_0'))
            #         ])
            #     ]
            # ),
            # (
            #     "var a; if (1 < 2) then let a <- 10; else let a <- 12; fi;",
            #     [],
            #     [
            #         Instructions([
            #             IRInstruction(OpConstant, ConstantOperand(value=1)),
            #             IRInstruction(OpConstant, ConstantOperand(value=2)),
            #             IRInstruction(OpCmp, AddressOperand(value=0), AddressOperand(value=1)),
            #             IRInstruction(OpGreaterEqual, AddressOperand(value=2), AddressOperand(value=7)),
            #             IRInstruction(OpConstant, ConstantOperand(value=10)),
            #             IRInstruction(OpLoad, LiteralOperand(value='GLOBAL@a_0')),
            #             IRInstruction(OpMove, AddressOperand(value=5), LiteralOperand(value='GLOBAL@a_0')),
            #             IRInstruction(OpBranch, AddressOperand(value=9)),
            #             IRInstruction(OpConstant, ConstantOperand(value=12)),
            #             IRInstruction(OpMove, AddressOperand(value=9), LiteralOperand(value='GLOBAL@a_1'))
            #         ])
            #     ]
            # ),
            # (
            #     "var a, b; let a <- 0; let b <- 0 if (1 < 5) then let a <- 10; else let b <- 12; fi; let a <- b; let b <- 2 + 3;",
            #     [],
            #     [
            #         Instructions([
            #             IRInstruction(OpConstant, ConstantOperand(value=1)),
            #             IRInstruction(OpConstant, ConstantOperand(value=2)),
            #             IRInstruction(OpCmp, AddressOperand(value=0), AddressOperand(value=1)),
            #             IRInstruction(OpGreaterEqual, AddressOperand(value=2), AddressOperand(value=7)),
            #             IRInstruction(OpConstant, ConstantOperand(value=10)),
            #             IRInstruction(OpLoad, LiteralOperand(value='GLOBAL@a_0')),
            #             IRInstruction(OpMove, AddressOperand(value=5), LiteralOperand(value='GLOBAL@a_0')),
            #             IRInstruction(OpBranch, AddressOperand(value=9)),
            #             IRInstruction(OpConstant, ConstantOperand(value=12)),
            #             IRInstruction(OpMove, AddressOperand(value=9), LiteralOperand(value='GLOBAL@a_1'))
            #         ])
            #     ]
            # ),
            # (
            #     "var a, b; "
            #     "if (a < b) then "
            #     "   var c;"
            #     "   if (b < a) then "
            #     "       let a <- 10; "
            #     "   else "
            #     "       let b <- 12; "
            #     "   fi; "
            #     "   let c <- 19;"
            #     "else "
            #     "   let b <- 13; "
            #     "fi;",
            #     [],
            #     [
            #         Instructions([
            #             IRInstruction(OpConstant, ConstantOperand(value=1)),
            #             IRInstruction(OpConstant, ConstantOperand(value=2)),
            #         ])
            #     ]
            # ),
            # (
            #     "var a, b;"
            #     "let b <- a + 42"
            #     "let b <- a < 4",
            #     [],
            #     []
            # ),
            # (
            #     "var a, b;"
            #     "while (a < b) do"
            #     "   let b <- a + 5"
            #     "od;",
            #     [],
            #     []
            # ),
            # (
            #     "var c;"
            #     "let c <- 99;"
            #     "function func(a);"
            #     "{"
            #     "   var b;"
            #     "   if (a < 5) then "
            #     "       let a <- 10; "
            #     "   else "
            #     "       let b <- 12; "
            #     "   fi;"
            #     "   let c <- a + b"
            #     "   return c + 6"
            #     "}"
            #     "let c <- 99;"
            #     "call func(5)",
            #     [],
            #     []
            # ),
            (
                "var i, j;"
                "array [ 4 ][ 2 ][ 5 ] arr;"
                "let arr[3][1][3] <- j",
                [],
                []
            ),
        ]

        self.compile_tests(tests)

    def compile_tests(self, tests: List[
        Tuple[str, List[Union[int, str, Instructions, IRInstruction]], List[Instructions]]]) -> None:
        for test in tests:
            program = self.parse(test[0])
            compiler = Compiler()

            err = compiler.compile(program)
            self.assertFalse(err)

            bytecode = compiler.byte_code

            control_flow_graph(compiler.byte_code)

            self.eval_instructions(test[2], bytecode.instructions)
            #
            # self.eval_constants(test[1], bytecode.constants)

    def parse(self, source: str) -> Program:
        l = Lexer(source)
        p = Parser(l)
        return p.parse_program()

    def eval_instructions(self, expected: List[Instructions], actual: Instructions) -> None:
        concat = self.concat_instructions(expected)
        self.assertEqual(len(concat), len(actual))

        for ins, act in zip(concat, actual):
            self.assertEqual(act, ins)

    @staticmethod
    def concat_instructions(s: List[Instructions]) -> Instructions:
        out = Instructions()
        for ins in s:
            out += ins
        return out

    def eval_constants(self, expected, actual: List[Object]) -> None:
        self.assertEqual(len(expected), len(actual))
        for exp, act in zip(expected, actual):
            if isinstance(act, CompiledFunctionObject):
                self.assertEqual(exp, act.instructions)
            else:
                self.assertEqual(exp, act.value)
