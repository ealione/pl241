# -*- encoding: utf-8 -*-
from copy import copy
from typing import Optional

from _exceptions import IllegalCharError, ExpectedCharError, Error
from tokens import Token, Tokens, Position


class Lexer(object):
    def __init__(self, source: str, filename: str = None):
        self.input = source
        self.position = Position()
        self.last_position = self.position
        self.cur_char = ''
        self.error: Optional[Error] = None
        self.filename = filename
        self.read_char()

    def read_char(self) -> None:
        if self.position.read >= len(self.input):
            self.cur_char = ''
        else:
            self.cur_char = self.input[self.position.read]
        self.position.advance()

    def peek_char(self) -> Optional[str]:
        if self.position.read >= len(self.input):
            return None
        else:
            return self.input[self.position.read]

    def read_identifier(self) -> str:
        self.last_position = copy(self.position)
        while self.cur_char.isalnum():
            self.read_char()
        return self.input[self.last_position.current: self.position.current]

    def read_number(self) -> str:
        self.last_position = copy(self.position)
        while self.cur_char.isnumeric():
            self.read_char()
        return self.input[self.last_position.current: self.position.current]

    def read_string(self) -> str:
        self.last_position = copy(self.position)
        self.last_position.advance()
        while True:
            self.read_char()
            if self.cur_char == '"' or self.cur_char is None:
                break
        return self.input[self.last_position.current: self.position.current]

    def skip_line(self) -> None:
        self.position.new_line()
        while not (self.cur_char == '\r' or self.cur_char == '\n'):
            self.read_char()

    def skip_whitespace(self) -> None:
        while self.cur_char == ' ' or self.cur_char == '\t' or self.cur_char == '\n' or self.cur_char == '\r':
            if self.cur_char == '\n':
                self.position.new_line()
            self.read_char()

    def get_next_token(self) -> Token:
        self.skip_whitespace()
        self.last_position = copy(self.position)

        if self.cur_char == '=':
            if self.peek_char() == '=':
                cur_char = self.cur_char
                self.read_char()
                literal = cur_char + self.cur_char
                t = Token.from_value(literal)
            else:
                self.error = ExpectedCharError(self.last_position, self.position, "'=' (after '=')", self.input,
                                               self.filename)
                t = Token.from_value(Tokens.ILLEGAL)
        elif self.cur_char == '!':
            if self.peek_char() == '=':
                cur_char = self.cur_char
                self.read_char()
                literal = cur_char + self.cur_char
                t = Token.from_value(literal)
            else:
                t = Token.from_value(self.cur_char)
        elif self.cur_char == '+':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '-':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '/':
            if self.peek_char() == '/':
                self.read_char()
                self.read_char()
                self.skip_whitespace()
                self.last_position = copy(self.position)
                self.skip_line()
                t = Token(type=Tokens.DOUBLESLASH,
                          literal=self.input[self.last_position.current: self.position.current])
            else:
                t = Token.from_value(self.cur_char)
        elif self.cur_char == '*':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '<':
            if self.peek_char() == '-':
                cur_char = self.cur_char
                self.read_char()
                literal = cur_char + self.cur_char
                t = Token.from_value(literal)
            elif self.peek_char() == '=':
                cur_char = self.cur_char
                self.read_char()
                literal = cur_char + self.cur_char
                t = Token.from_value(literal)
            else:
                t = Token.from_value(self.cur_char)
        elif self.cur_char == '>':
            if self.peek_char() == '=':
                cur_char = self.cur_char
                self.read_char()
                literal = cur_char + self.cur_char
                t = Token.from_value(literal)
            else:
                t = Token.from_value(self.cur_char)
        elif self.cur_char == '.':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == ';':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == ':':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == ',':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '{':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '}':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '(':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == ')':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '[':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == ']':
            t = Token.from_value(self.cur_char)
        elif self.cur_char == '"':
            t = Token(type=Tokens.STRING, literal=self.read_string())
        elif self.cur_char == '#':
            self.read_char()
            self.skip_whitespace()
            self.last_position = copy(self.position)
            self.skip_line()
            t = Token(type=Tokens.HASH,
                      literal=self.input[self.last_position.current: self.position.current])
        elif self.cur_char == '':
            t = Token.from_value(Tokens.EOF)
        else:
            if self.cur_char.isalpha():
                return Token.from_value(self.read_identifier())
            elif self.cur_char.isnumeric():
                return Token(type=Tokens.INT, literal=self.read_number())
            else:
                self.position.advance()
                self.error = IllegalCharError(self.last_position, self.position, f"'{self.cur_char}'", self.input,
                                              self.filename)
                t = Token(type=Tokens.ILLEGAL, literal=self.cur_char)

        self.read_char()
        return t
