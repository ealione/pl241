# -*- encoding: utf-8 -*-
from unittest import TestCase

from lexer import Lexer
from tokens import Tokens


class TestLexer(TestCase):

    def test_next_char(self):
        input = '' \
                'main' \
                ' ' \
                'let five <- 5;' \
                'let ten <- 10;' \
                'var x, y;' \
                'call FunctionName(argument)' \
                'array [ 5 ] a;' \
                'function foo( );' \
                'while i < 10 do' \
                ' ' \
                'procedure bar( x, z );' \
                '{' \
                '   if i >= 4 then' \
                '       k <- 0' \
                '   else' \
                '       //        comment\n' \
                '       # another comment\n' \
                '   fi;' \
                '}'

        l = Lexer(input)

        tests = [(Tokens.MAIN, "main"),
                 (Tokens.LET, "let"),
                 (Tokens.IDENT, "five"),
                 (Tokens.ASSIGN, "<-"),
                 (Tokens.INT, "5"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.LET, "let"),
                 (Tokens.IDENT, "ten"),
                 (Tokens.ASSIGN, "<-"),
                 (Tokens.INT, "10"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.VAR, "var"),
                 (Tokens.IDENT, "x"),
                 (Tokens.COMMA, ","),
                 (Tokens.IDENT, "y"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.CALL, "call"),
                 (Tokens.IDENT, "FunctionName"),
                 (Tokens.LPAREN, "("),
                 (Tokens.IDENT, "argument"),
                 (Tokens.RPAREN, ")"),
                 (Tokens.ARRAY, "array"),
                 (Tokens.LBRACKET, "["),
                 (Tokens.INT, "5"),
                 (Tokens.RBRACKET, "]"),
                 (Tokens.IDENT, "a"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.FUNCTION, "function"),
                 (Tokens.IDENT, "foo"),
                 (Tokens.LPAREN, "("),
                 (Tokens.RPAREN, ")"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.WHILE, "while"),
                 (Tokens.IDENT, "i"),
                 (Tokens.LT, "<"),
                 (Tokens.INT, "10"),
                 (Tokens.DO, "do"),
                 (Tokens.PROCEDURE, "procedure"),
                 (Tokens.IDENT, "bar"),
                 (Tokens.LPAREN, "("),
                 (Tokens.IDENT, "x"),
                 (Tokens.COMMA, ","),
                 (Tokens.IDENT, "z"),
                 (Tokens.RPAREN, ")"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.LBRACE, "{"),
                 (Tokens.IF, "if"),
                 (Tokens.IDENT, "i"),
                 (Tokens.GEQT, ">="),
                 (Tokens.INT, "4"),
                 (Tokens.THEN, "then"),
                 (Tokens.IDENT, "k"),
                 (Tokens.ASSIGN, "<-"),
                 (Tokens.INT, "0"),
                 (Tokens.ELSE, "else"),
                 (Tokens.DOUBLESLASH, "comment"),
                 (Tokens.HASH, "another comment"),
                 (Tokens.FI, "fi"),
                 (Tokens.SEMICOLON, ";"),
                 (Tokens.RBRACE, "}"),
                 (Tokens.EOF, "EOF")]
        for t_type, t_literal in tests:
            n_t = l.get_next_token()
            self.assertFalse(l.error)
            self.assertEqual(t_literal, n_t.literal) and self.assertEqual(t_type, n_t.type)
