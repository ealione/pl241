# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from typing import NewType, Final, List, Any

from ast import *
from compiler.symbol_table import Symbol
from objects import Environment

ObjectType = NewType('ObjectType', str)

NULL_OBJ: Final[ObjectType] = ObjectType("NULL")
ERROR_OBJ: Final[ObjectType] = ObjectType("ERROR")
INTEGER_OBJ: Final[ObjectType] = ObjectType("INTEGER")
BOOLEAN_OBJ: Final[ObjectType] = ObjectType("BOOLEAN")
STRING_OBJ: Final[ObjectType] = ObjectType("STRING")
RETURN_VALUE_OBJ: Final[ObjectType] = ObjectType("RETURN_VALUE")
FUNCTION_OBJ: Final[ObjectType] = ObjectType("FUNCTION")
BUILTIN_OBJ: Final[ObjectType] = ObjectType("BUILTIN")
ARRAY_OBJ: Final[ObjectType] = ObjectType("ARRAY")
ARRAY_DECLARATION_OBJ: Final[ObjectType] = ObjectType("ARRAY_DECLARATION")
HASH_OBJ: Final[ObjectType] = ObjectType("HASH")
COMPILED_FUNCTION_OBJ: Final[ObjectType] = ObjectType("COMPILED_FUNCTION_OBJ")
CLOSURE_OBJ: Final[ObjectType] = ObjectType("CLOSURE")


@dataclass
class Hash_key:
    type: ObjectType
    value: int


@dataclass
class Hashable:
    def __hash__(self) -> int:
        return NotImplemented


@dataclass
class Object:
    type: ObjectType

    def __str__(self) -> str:
        return NotImplemented


@dataclass
class IntegerObject(Object):
    value: int = field(default=0)
    type: ObjectType = field(default=INTEGER_OBJ)

    def __str__(self) -> str:
        return f"{self.value}"

    def __hash__(self) -> int:
        return hash((self.type, self.value))


@dataclass
class NullObject(Object):
    type: ObjectType = field(default=NULL_OBJ)

    def __str__(self) -> str:
        return "null"


@dataclass
class ReturnValueObject(Object):
    value: Object = field(default_factory=None)
    type: ObjectType = field(default=RETURN_VALUE_OBJ)

    def __str__(self) -> str:
        return f"{str(self.value)}"


@dataclass
class ErrorObject(Object):
    message: str = field(default="")
    type: ObjectType = field(default=ERROR_OBJ)

    def __str__(self) -> str:
        return f"ERROR: {self.message}"


@dataclass
class FunctionObject(Object):
    parameters: List[Identifier] = field(default_factory=list)
    body: BlockStatement = field(default_factory=BlockStatement)
    env: Environment = field(default_factory=Environment)
    type: ObjectType = field(default=FUNCTION_OBJ)

    def __str__(self) -> str:
        return f"fn ({self.parameters}) {{\n{str(self.body)}\n}}"


@dataclass
class StringObject(Object):
    value: str = field(default="")
    type: ObjectType = field(default=STRING_OBJ)

    def __str__(self) -> str:
        return f"{self.value}"

    def __hash__(self) -> int:
        return hash((self.type, self.value))


@dataclass
class ArrayObject(Object):
    elements: List[Object] = field(default_factory=list)
    type: ObjectType = field(default=ARRAY_OBJ)

    def __str__(self) -> str:
        elements = ','.join(f"{str(o)}" for o in self.elements)
        return f"[{elements}]"


@dataclass
class ArrayDeclarationObject(Object):
    symbol: Symbol = field(default_factory=Symbol)
    dimensions: List[Object] = field(default_factory=list)
    type: ObjectType = field(default=ARRAY_DECLARATION_OBJ)

    def __str__(self) -> str:
        dimensions = ','.join(f"{str(o)}" for o in self.dimensions)
        return f"{self.name}[{dimensions}]"


@dataclass
class CompiledFunctionObject(Object):
    locals_count: int = field(default=0)
    parameters_count: int = field(default=0)
    parameters: List = field(default_factory=list)
    bytecode: Any = field(default_factory=None)
    type: ObjectType = field(default=COMPILED_FUNCTION_OBJ)
    symbol: Symbol = field(default_factory=Symbol)

    def __str__(self) -> str:
        return f"Function with [{self.locals_count}] local variables and [{self.parameters_count}] parameters" \
               f"\n{str(self.bytecode.instructions)}"


@dataclass
class CompiledProcedureObject(Object):
    locals_count: int = field(default=0)
    parameters_count: int = field(default=0)
    parameters: List = field(default_factory=list)
    bytecode: Any = field(default_factory=None)
    type: ObjectType = field(default=COMPILED_FUNCTION_OBJ)
    symbol: Symbol = field(default_factory=Symbol)

    def __str__(self) -> str:
        return f"Procedure with [{self.locals_count}] local variables and [{self.parameters_count}] parameters" \
               f"\n{str(self.bytecode.instructions)}"


@dataclass
class ClosureObject(Object):
    fn: CompiledFunctionObject = field(default_factory=CompiledFunctionObject)
    free: List[Object] = field(default_factory=list)
    type: ObjectType = field(default=CLOSURE_OBJ)

    def __str__(self) -> str:
        return f"Closure"
