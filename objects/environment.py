# -*- encoding: utf-8 -*-
from __future__ import annotations

from dataclasses import dataclass, field
from typing import Dict, Tuple

import objects


@dataclass
class Environment:
    store: Dict[str, objects.Object] = field(default_factory=dict)
    outer: Environment = field(default_factory=None)

    @classmethod
    def enclosed_environment(cls, outer: Environment):
        return cls(outer=outer)

    def get(self, name: str) -> Tuple[objects.Object, bool]:
        obj, ok = self.store.get[name, None], name in self.store
        if not ok and self.outer is not None:
            obj, ok = self.outer.get(name)
        return obj, ok

    def set(self, name: str, val: objects.Object) -> objects.Object:
        self.store[name] = val
        return val
